<div class="families form form-horizontal">
<?php echo $this->Form->create('Family'); ?>
	<fieldset>
		<legend><?php echo __('Edit Family'); ?></legend>
			<div class="form-group">
	<label class='col-lg-2 control-label'>id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>type</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('type', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>name</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('name', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>lastname</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('lastname', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>born_date</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('born_date', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>age</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('age', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>dni</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('dni', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>phone</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('phone', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>celphone</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('celphone', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>instruction_grade</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('instruction_grade', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>profession</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('profession', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>job</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('job', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>occupation</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('occupation', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>essalud</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('essalud', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>entitled</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('entitled', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>worker_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>company_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('company_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>company_job</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('company_job', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>company_number</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('company_number', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

