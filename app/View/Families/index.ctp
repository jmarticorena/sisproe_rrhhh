<div class="families index">
	<h2><?php echo __('Families'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('lastname'); ?></th>
			<th><?php echo $this->Paginator->sort('born_date'); ?></th>
			<th><?php echo $this->Paginator->sort('age'); ?></th>
			<th><?php echo $this->Paginator->sort('dni'); ?></th>
			<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('celphone'); ?></th>
			<th><?php echo $this->Paginator->sort('instruction_grade'); ?></th>
			<th><?php echo $this->Paginator->sort('profession'); ?></th>
			<th><?php echo $this->Paginator->sort('job'); ?></th>
			<th><?php echo $this->Paginator->sort('occupation'); ?></th>
			<th><?php echo $this->Paginator->sort('essalud'); ?></th>
			<th><?php echo $this->Paginator->sort('entitled'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('company_job'); ?></th>
			<th><?php echo $this->Paginator->sort('company_number'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($families as $family): ?>
	<tr>
		<td><?php echo h($family['Family']['id']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['type']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['name']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['lastname']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['born_date']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['age']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['dni']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['phone']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['celphone']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['instruction_grade']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['profession']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['job']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['occupation']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['essalud']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['entitled']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($family['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $family['Worker']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($family['Company']['name'], array('controller' => 'companies', 'action' => 'view', $family['Company']['id'])); ?>
		</td>
		<td><?php echo h($family['Family']['company_job']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['company_number']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['created']); ?>&nbsp;</td>
		<td><?php echo h($family['Family']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $family['Family']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $family['Family']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $family['Family']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $family['Family']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

