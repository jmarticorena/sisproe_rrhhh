<div class="families view">
<h2><?php echo __('Family'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($family['Family']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($family['Family']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($family['Family']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lastname'); ?></dt>
		<dd>
			<?php echo h($family['Family']['lastname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Born Date'); ?></dt>
		<dd>
			<?php echo h($family['Family']['born_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Age'); ?></dt>
		<dd>
			<?php echo h($family['Family']['age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dni'); ?></dt>
		<dd>
			<?php echo h($family['Family']['dni']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($family['Family']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Celphone'); ?></dt>
		<dd>
			<?php echo h($family['Family']['celphone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instruction Grade'); ?></dt>
		<dd>
			<?php echo h($family['Family']['instruction_grade']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Profession'); ?></dt>
		<dd>
			<?php echo h($family['Family']['profession']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Job'); ?></dt>
		<dd>
			<?php echo h($family['Family']['job']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Occupation'); ?></dt>
		<dd>
			<?php echo h($family['Family']['occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Essalud'); ?></dt>
		<dd>
			<?php echo h($family['Family']['essalud']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Entitled'); ?></dt>
		<dd>
			<?php echo h($family['Family']['entitled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($family['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $family['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($family['Company']['name'], array('controller' => 'companies', 'action' => 'view', $family['Company']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Job'); ?></dt>
		<dd>
			<?php echo h($family['Family']['company_job']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Number'); ?></dt>
		<dd>
			<?php echo h($family['Family']['company_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($family['Family']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($family['Family']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

