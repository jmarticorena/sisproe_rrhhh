<div class="medicalExams view">
<h2><?php echo __('Exámen Médico'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($medicalExam['MedicalExam']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Válido del'); ?></dt>
		<dd>
			<?php echo h($medicalExam['MedicalExam']['begin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hasta'); ?></dt>
		<dd>
			<?php echo h($medicalExam['MedicalExam']['end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Trabajador'); ?></dt>
		<dd>
			<?php echo $this->Html->link($medicalExam['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $medicalExam['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($medicalExam['MedicalExam']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($medicalExam['MedicalExam']['updated']); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>

