<div class="medicalExams form form-horizontal">
<?php echo $this->Form->create('MedicalExam'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Exámen Médico'); ?></legend>
			<div class="form-group">
	<label class='col-lg-2 control-label'>Válido del</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('begin', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control', 'type' => 'text', 'class' => 'form-control', 'id'=>'datetimepicker1', 'dateFormat' => 'DMY')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Hasta</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('end', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control', 'type' => 'text', 'class' => 'form-control', 'id'=>'datetimepicker2', 'dateFormat' => 'DMY')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Trabajador</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control', 'empty'=>true)); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

<script>
	$('#datetimepicker1').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat : 'yy/mm/dd',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
	});

	$('#datetimepicker2').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat : 'yy/mm/dd',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
	});

	$("#MedicalExamWorkerId").select2({
		placeholder: "Selecciona un Trabajador",
		language: "es",
		tags: true,
		single: true,
	});

</script>