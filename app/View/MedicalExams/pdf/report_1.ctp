<div class="medicalExams index">
	<h2><?php echo __('Exámenes Médicos - Reporte'); ?></h2>
	
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo h('id'); ?></th> -->
			<th><?php echo h('Válido del'); ?></th>
			<th><?php echo h('Hasta'); ?></th>
			<th><?php echo h('Tiempo Restante'); ?></th>
			<th><?php echo h('Trabajador'); ?></th>
			<!-- <th><?php echo h('created'); ?></th>
			<th><?php echo h('updated'); ?></th> -->
			
	</tr>
	</thead>
	<tbody>
	<?php
	$limit = 1; # 1 mes
	$green = '#449d44';
	$orange = '#f0ad4e';
	$red =  '#c9302c';
	?>
	<?php foreach ($medicalExams as $medicalExam): ?>
		<?php 
		$actual = date('Y-m-d');

		$begin = $medicalExam['MedicalExam']['begin'];
		$end = $medicalExam['MedicalExam']['end'];

		$diff = abs(strtotime($end) - strtotime($actual));

		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		if (strtotime($end) > strtotime($actual)) {
			# falta para vencerse
			$state = $orange; #falta poco para vencerse
			if ($months >= $limit){
				$state = $green; #falta mucho para vencerse
			}
		}
		else{
			# se vencio
			$state = $red;
		}

		?>
	<?php echo "<tr>"; ?>
		<!-- <td><?php echo h($medicalExam['MedicalExam']['id']); ?>&nbsp;</td> -->
		<td><font color="#000"><?php echo h($begin); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($end); ?>&nbsp;</font></td>
		<td><font color="#000"><?php printf("%d años, %d meses, %d días", $years, $months, $days); ?>&nbsp;</td>
		<td><font color="#000">
			<?php echo h($medicalExam['Worker']['name']); ?>
		</font></td>
		<!-- <td><?php echo h($medicalExam['MedicalExam']['created']); ?>&nbsp;</td>
		<td><?php echo h($medicalExam['MedicalExam']['updated']); ?>&nbsp;</td> -->
		
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">


  
</div>
</div>

