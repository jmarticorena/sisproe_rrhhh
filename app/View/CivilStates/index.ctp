<div class="civilStates index">
	<h2><?php echo __('Civil States'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($civilStates as $civilState): ?>
	<tr>
		<td><?php echo h($civilState['CivilState']['id']); ?>&nbsp;</td>
		<td><?php echo h($civilState['CivilState']['name']); ?>&nbsp;</td>
		<td><?php echo h($civilState['CivilState']['created']); ?>&nbsp;</td>
		<td><?php echo h($civilState['CivilState']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $civilState['CivilState']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $civilState['CivilState']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $civilState['CivilState']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $civilState['CivilState']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

