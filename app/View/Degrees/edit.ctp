<div class="degrees form form-horizontal">
<?php echo $this->Form->create('Degree'); ?>
	<fieldset>
		<legend><?php echo __('Edit Degree'); ?></legend>
			<div class="form-group">
	<label class='col-lg-2 control-label'>id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>worker_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>instruction_grade_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('instruction_grade_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>institution_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('institution_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>career_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('career_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

