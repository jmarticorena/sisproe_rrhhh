<div class="degrees view">
<h2><?php echo __('Degree'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($degree['Degree']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($degree['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $degree['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instruction Grade'); ?></dt>
		<dd>
			<?php echo $this->Html->link($degree['InstructionGrade']['name'], array('controller' => 'instruction_grades', 'action' => 'view', $degree['InstructionGrade']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Institution'); ?></dt>
		<dd>
			<?php echo $this->Html->link($degree['Institution']['name'], array('controller' => 'institutions', 'action' => 'view', $degree['Institution']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Career Id'); ?></dt>
		<dd>
			<?php echo h($degree['Degree']['career_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($degree['Degree']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($degree['Degree']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

