<div class="degrees index">
	<h2><?php echo __('Degrees'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('instruction_grade_id'); ?></th>
			<th><?php echo $this->Paginator->sort('institution_id'); ?></th>
			<th><?php echo $this->Paginator->sort('career_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($degrees as $degree): ?>
	<tr>
		<td><?php echo h($degree['Degree']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($degree['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $degree['Worker']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($degree['InstructionGrade']['name'], array('controller' => 'instruction_grades', 'action' => 'view', $degree['InstructionGrade']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($degree['Institution']['name'], array('controller' => 'institutions', 'action' => 'view', $degree['Institution']['id'])); ?>
		</td>
		<td><?php echo h($degree['Degree']['career_id']); ?>&nbsp;</td>
		<td><?php echo h($degree['Degree']['created']); ?>&nbsp;</td>
		<td><?php echo h($degree['Degree']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $degree['Degree']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $degree['Degree']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $degree['Degree']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $degree['Degree']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

