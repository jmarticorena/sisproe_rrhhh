<div class="medicalRegisters index">
	<h2><?php echo __('Medical Registers'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('accident'); ?></th>
			<th><?php echo $this->Paginator->sort('disease'); ?></th>
			<th><?php echo $this->Paginator->sort('family'); ?></th>
			<th><?php echo $this->Paginator->sort('allergy'); ?></th>
			<th><?php echo $this->Paginator->sort('blood'); ?></th>
			<th><?php echo $this->Paginator->sort('emergency_contact'); ?></th>
			<th><?php echo $this->Paginator->sort('emergency_number'); ?></th>
			<th><?php echo $this->Paginator->sort('emergency_adress'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($medicalRegisters as $medicalRegister): ?>
	<tr>
		<td><?php echo h($medicalRegister['MedicalRegister']['id']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['accident']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['disease']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['family']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['allergy']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['blood']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['emergency_contact']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['emergency_number']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['emergency_adress']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($medicalRegister['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $medicalRegister['Worker']['id'])); ?>
		</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['created']); ?>&nbsp;</td>
		<td><?php echo h($medicalRegister['MedicalRegister']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $medicalRegister['MedicalRegister']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $medicalRegister['MedicalRegister']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $medicalRegister['MedicalRegister']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $medicalRegister['MedicalRegister']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

