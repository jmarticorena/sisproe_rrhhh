<div class="medicalRegisters view">
<h2><?php echo __('Medical Register'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accident'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['accident']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Disease'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['disease']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Family'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['family']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Allergy'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['allergy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Blood'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['blood']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Emergency Contact'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['emergency_contact']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Emergency Number'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['emergency_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Emergency Adress'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['emergency_adress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($medicalRegister['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $medicalRegister['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($medicalRegister['MedicalRegister']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

