<div class="medicalRegisters form form-horizontal">
<?php echo $this->Form->create('MedicalRegister'); ?>
	<fieldset>
		<legend><?php echo __('Edit Medical Register'); ?></legend>
			<div class="form-group">
	<label class='col-lg-2 control-label'>id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>accident</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('accident', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>disease</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('disease', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>family</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('family', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>allergy</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('allergy', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>blood</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('blood', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>emergency_contact</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('emergency_contact', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>emergency_number</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('emergency_number', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>emergency_adress</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('emergency_adress', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>worker_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

