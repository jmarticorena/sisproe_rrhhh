<div class="costCenters index">
	<h2><?php echo __('Centros de Costo'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('code', 'Código'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($costCenters as $costCenter): ?>
	<tr>
		<!-- <td><?php echo h($costCenter['CostCenter']['id']); ?>&nbsp;</td> -->
		<td><?php echo h($costCenter['CostCenter']['name']); ?>&nbsp;</td>
		<td><?php echo h($costCenter['CostCenter']['code']); ?>&nbsp;</td>
		<!-- <td><?php echo h($costCenter['CostCenter']['created']); ?>&nbsp;</td>
		<td><?php echo h($costCenter['CostCenter']['updated']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $costCenter['CostCenter']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $costCenter['CostCenter']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $costCenter['CostCenter']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $costCenter['CostCenter']['name']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

