<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Sisproe - Recursos Humanos</title>
	<?php
		echo $this->Html->meta('icon');

		# echo $this->Html->css('cake.generic');
		echo $this->Html->css('jquery-ui');
		echo $this->Html->css('jquery.datetimepicker');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('custom');
		echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('select2-4/select2.min');

		echo $this->Html->script('jquery.min');
		echo $this->Html->script('jquery-ui.min');
		echo $this->Html->script('jquery.datetimepicker');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('select2-4.0.0/dist/js/select2.full');
		echo $this->Html->script('select2-4.0.0/dist/js/prettify.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-inverse  navbar-fixed-top" role="navigation">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		    	    <span class="sr-only">Toggle navigation</span>
		    	    <span class="icon-bar"></span>
		    	    <span class="icon-bar"></span>
		    	    <span class="icon-bar"></span>
		    	</button>
		        <a class="navbar-brand" href="/">Sisproe - Recursos Humanos</a>
		    </div>
		   
		    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
		    <div class="collapse navbar-collapse navbar-ex1-collapse">
		        <ul class="nav navbar-nav side-nav">
					
		        	<li class="disabled"><a href="#">Gestión</a></li>
		        	<?php
		        		if ($controller == 'workers' && $action != 'report' && $action != 'report_1' && $action != 'report_2' && $action != 'report_3'){
		        			echo "<li class='active'>";
		        		}
		        		else{
		        			echo "<li>";
		        		}
		        	?>
		        	    <a href="javascript:;" data-toggle="collapse" data-target="#workers"><i class="fa fa-fw fa-users"></i> Trabajadores <i class="fa fa-fw fa-caret-down"></i></a>
		        	    <?php
		        	    if ($controller == 'workers' && $action != 'report' && $action != 'report_1' && $action != 'report_2' && $action != 'report_3'){
		        	    	echo "<ul id='workers' class='collapse in'>";
		        	    }
		        	    else{
		        	    	echo "<ul id='workers' class='collapse'>";
		        	    }
		        	    ?>
							<li>
		        	            <a href="/workers/index">Listar</a>
		        	        </li>
		        	        <li>
		        	            <a href="/workers/add">Agregar</a>
		        	        </li>
		        	        <li>
		        	            <a href="/work_experiences/add">Agregar Experiencia Laboral</a>
		        	        </li>
		        	        <li>
		        	            <a href="/degrees/add">Agregar Estudios</a>
		        	        </li>
		        	        <li>
		        	            <a href="/legacies/add">Agregar Hijos</a>
		        	        </li>
		        	    </ul>

		        	<?php
		        		if ($controller == 'contracts'  && $action != 'report' && $action != 'report_1' && $action != 'report_2' && $action != 'report_3'){
		        			echo "<li class='active'>";
		        		}
		        		else{
		        			echo "<li>";
		        		}
		        	?>
		        	    <a href="javascript:;" data-toggle="collapse" data-target="#contracts"><i class="fa fa-fw fa-usd"></i> Contratos <i class="fa fa-fw fa-caret-down"></i></a>
		        	    <?php
		        	    if ($controller == 'contracts'  && $action != 'report' && $action != 'report_1' && $action != 'report_2' && $action != 'report_3'){
		        	    	echo "<ul id='contracts' class='collapse in'>";
		        	    }
		        	    else{
		        	    	echo "<ul id='contracts' class='collapse'>";
		        	    }
		        	    ?>
							<li>
		        	            <a href="/contracts/index">Listar</a>
		        	        </li>
		        	        <li>
		        	            <a href="/contracts/add">Agregar</a>
		        	        </li>
		        	    </ul>
		        	<?php
		        		if (($controller == 'medical_exams' || $controller == 'insurances') && ($action != 'report' && $action != 'report_1' && $action != 'report_2' && $action != 'report_3')){
		        			echo "<li class='active'>";
		        		}
		        		else{
		        			echo "<li>";
		        		}
		        	?>
		        	    <a href="javascript:;" data-toggle="collapse" data-target="#medical"><i class="fa fa-fw fa-ambulance"></i> Exámenes Médicos y Seguros <i class="fa fa-fw fa-caret-down"></i></a>
		        	    <?php
		        	    if (($controller == 'medical_exams' || $controller == 'insurances') && ($action == 'index' || $action == 'add')){
		        	    	echo "<ul id='medical' class='collapse in'>";
		        	    }
		        	    else{
		        	    	echo "<ul id='medical' class='collapse'>";
		        	    }
		        	    ?>
							<li>
		        	            <a href="/medical_exams/index">Listar exámenes médicos</a>
		        	        </li>
		        	        <li>
		        	            <a href="/medical_exams/add">Agregar exámenes médicos</a>
		        	        </li>
		        	        <li>
		        	            <a href="/insurances/index">Listar seguros</a>
		        	        </li>
		        	        <li>
		        	            <a href="/insurances/add">Agregar seguros</a>
		        	        </li>

		        	    </ul>

		            <?php
		        		if ($controller == 'cost_centers'){
		        			echo "<li class='active'>";
		        		}
		        		else{
		        			echo "<li>";
		        		}
		        	?>
		        	    <a href="javascript:;" data-toggle="collapse" data-target="#cost_centers"><i class="fa fa-fw fa-building"></i> Centros de Costo <i class="fa fa-fw fa-caret-down"></i></a>
		        	    <?php
		        	    if ($controller == 'cost_centers'){
		        	    	echo "<ul id='cost_centers' class='collapse in'>";
		        	    }
		        	    else{
		        	    	echo "<ul id='cost_centers' class='collapse'>";
		        	    }
		        	    ?>
							<li>
		        	            <a href="/cost_centers/index">Listar</a>
		        	        </li>
		        	        <li>
		        	            <a href="/cost_centers/add">Agregar</a>
		        	        </li>
		        	    </ul>

					<?php
		        		if ($controller == 'clients'){
		        			echo "<li class='active'>";
		        		}
		        		else{
		        			echo "<li>";
		        		}
		        	?>
		        	    <a href="javascript:;" data-toggle="collapse" data-target="#clients"><i class="fa fa-fw fa-child"></i> Clientes <i class="fa fa-fw fa-caret-down"></i></a>
		        	    <?php
		        	    if ($controller == 'clients'){
		        	    	echo "<ul id='clients' class='collapse in'>";
		        	    }
		        	    else{
		        	    	echo "<ul id='clients' class='collapse'>";
		        	    }
		        	    ?>
							<li>
		        	            <a href="/clients/index">Listar</a>
		        	        </li>
		        	        <li>
		        	            <a href="/clients/add">Agregar</a>
		        	        </li>
		        	    </ul>

		            <?php
		        		if ($controller == 'projects'){
		        			echo "<li class='active'>";
		        		}
		        		else{
		        			echo "<li>";
		        		}
		        	?>
		        	    <a href="javascript:;" data-toggle="collapse" data-target="#projects"><i class="fa fa-fw fa-file"></i> Proyectos <i class="fa fa-fw fa-caret-down"></i></a>
		        	    <?php
		        	    if ($controller == 'projects'){
		        	    	echo "<ul id='projects' class='collapse in'>";
		        	    }
		        	    else{
		        	    	echo "<ul id='projects' class='collapse'>";
		        	    }
		        	    ?>
							<li>
		        	            <a href="/projects/index">Listar</a>
		        	        </li>
		        	        <li>
		        	            <a href="/projects/add">Agregar</a>
		        	        </li>
		        	    </ul>


			            <li class="disabled"><a href="#">Reportes</a></li>
			            <?php
				            if ($controller == 'medical_exams' && ($action == 'report' || $action == 'report_1' || $action == 'report_2' || $action == 'report_3')){
				            	echo "<li class='active'>";
				            }
				            else{
				            	echo "<li>";
				            }
			            ?>
			                <a href="/medical_exams/report"><i class="fa fa-fw fa-ambulance"></i> Exámenes Médicos</a>
			            </li>
			            <?php
				            if ($controller == 'insurances' && ($action == 'report' || $action == 'report_1' || $action == 'report_2' || $action == 'report_3')){
				            	echo "<li class='active'>";
				            }
				            else{
				            	echo "<li>";
				            }
			            ?>
			                <a href="/insurances/report"><i class="fa fa-fw fa-ambulance"></i> Seguros</a>
			            </li>
			             <?php
				            if ($controller == 'contracts' && ($action == 'report' || $action == 'report_1' || $action == 'report_2' || $action == 'report_3')){
				            	echo "<li class='active'>";
				            }
				            else{
				            	echo "<li>";
				            }
			            ?>
			                <a href="/contracts/report"><i class="fa fa-fw fa-users"></i> Contratos </a>
			            </li>

		        </ul>
		    </div>
		    <!-- /.navbar-collapse -->
		</nav>

		<div id="page-wrapper">
				<div class="container-fluid">
					<?php echo $this->Session->flash(); ?>

					<div class="row">
						<div class="col-lg-12">
						<?php echo $this->fetch('content'); ?>
						</div>

					</div>

				</div>
		</div>
		
	</div>
</body>
</html>

