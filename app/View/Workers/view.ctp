<div class="workers view">
<h2><?php echo __('Trabajador'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('DNI'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['dni']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Apellido'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['lastname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha de nacimiento'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['born_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lugar de nacimiento'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['born_place']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Edad'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Teléfono'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Celular'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['celphone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dirección'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Policlínico'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['polyclinic']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Policlínico-Otros'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['polyclinic_other']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AFP'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['afp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CUSP'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['cusp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ONP'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['onp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Brevete y Categoría'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['license_category']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha de Caducidad'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['expiration_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('N° de Cuenta de Haberes'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['assets_account_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Banco'); ?></dt>
		<dd>
			<?php echo $this->Html->link($worker['Bank']['name'], array('controller' => 'banks', 'action' => 'view', $worker['Bank']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Distrito'); ?></dt>
		<dd>
			<?php echo $this->Html->link($worker['District']['name'], array('controller' => 'districts', 'action' => 'view', $worker['District']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Estado Cívil'); ?></dt>
		<dd>
			<?php echo $this->Html->link($worker['CivilState']['name'], array('controller' => 'civil_states', 'action' => 'view', $worker['CivilState']['id'])); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($worker['Worker']['updated']); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>

