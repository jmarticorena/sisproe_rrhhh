  <!-- Nav tabs -->
	  <ul id='myTabs' class="nav nav-tabs" role="tablist">
	    <li role="presentation"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Área Personal</a></li>
	    <!-- <li role="presentation"><a href="#studies" aria-controls="studies" role="tab" data-toggle="tab">Estudios </a></li> -->
	    <li role="presentation"><a href="#epps" aria-controls="epps" role="tab" data-toggle="tab">EPPS Tallas</a></li>
	    <!-- <li role="presentation"><a href="#experience" aria-controls="experience" role="tab" data-toggle="tab">Experencia Laboral</a></li> -->
	    <li role="presentation"><a href="#family" aria-controls="family" role="tab" data-toggle="tab">Área Familiar</a></li>
	    <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Área Vivienda</a></li>
	    <li role="presentation"><a href="#health" aria-controls="health" role="tab" data-toggle="tab">Área Salud</a></li>
	    <li role="presentation"><a href="#social" aria-controls="social" role="tab" data-toggle="tab">Área Social</a></li>
	  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
  	<div role="tabpanel" class="tab-pane fade active" id="personal">
  		<br>
  		<div class="workers form form-horizontal">
  			<?php echo $this->Form->create(); ?>
  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Nombres</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.name', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Apellidos</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.lastname', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>DNI</label>
  						<div class="col-lg-5">
  							<?php echo $this->Form->input('Worker.dni', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>

  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Fecha de Nacimiento</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.born_date', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control', 'id'=>'datetimepicker1', 'dateFormat' => 'DMY')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Lugar de Nacimiento</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.born_place', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Edad</label>
  						<div class="col-lg-3">
  							<?php echo $this->Form->input('Worker.age', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Teléfono</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.phone', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Celular</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.celphone', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			
  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Departamento</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('department_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control', 'empty'=>true)); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Provincia</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('province_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control', 'empty'=>true)); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Distrito</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.district_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control', 'empty'=>true)); ?>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Domicilio Actual</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.address', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Correos Electrónicos</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.email', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Estado Civil</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.civil_state_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control', 'empty'=>true)); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Policlínico ESSAULD (donde se atiende)</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.polyclinic', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Otro</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.polyclinic_other', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>AFP</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.afp', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>ONP</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.onp', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>CUSP</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.cusp', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Brevete y Categoría</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.license_category', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Fecha de Caducidad</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.expiration_date', array(
  							'div'=>false, 'label'=>false, 'type' => 'text', 'class' => 'form-control', 'id'=>'datetimepicker2', 'dateFormat' => 'DMY')); ?>
  						</div>
  					</div>
  				</div>
  			</div>

  			<div class="row">
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>N° de Cuenta de Haberes</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.assets_account_number', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-4">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Banco</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Worker.bank_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control', 'empty'=>true)); ?>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col-md-offset-5 col-md-3">
  					<a class="btn btn-primary btnNext">Siguiente</a>
  				</div>
  			</div>
  			
  		</div>
  	</div>

  	<div role="tabpanel" class="tab-pane fade" id="studies">
  		<br>
  		<div class="studies form form-horizontal" id="studies_forms">

  			<div class="row">
  				<div class="col-md-6">
					<div class="form-group">
						<label class='col-lg-4 control-label'>Carrera</label>
						<div class="col-lg-8">
							<?php echo $this->Form->input('Degree.0.career_id', array(
							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
						</div>
					</div>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col-md-6">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Grado de Instrucción</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Degree.0.instruction_grade_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col-md-6">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Año de egreso</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Degree.0.egress_date', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>
  			<div class="row">
  				<div class="col-md-6">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Institución</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Degree.0.institution_id', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  				<div class="col-md-6">
  					<div class="form-group">
  						<label class='col-lg-4 control-label'>Régimen de la Institución</label>
  						<div class="col-lg-8">
  							<?php echo $this->Form->input('Institution.0.regime', array(
  							'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
  						</div>
  					</div>
  				</div>
  			</div>
  			
  		</div>
  		<a class="btn btn-primary add_study">+</a>

  		<div class="col-md-offset-5 col-md-3">
  			<a class="btn btn-primary btnPrevious">Anterior</a>
  			<a class="btn btn-primary btnNext">Siguiente</a>
  			
  		</div>
  		
  	</div>
  	<div role="tabpanel" class="tab-pane fade" id="epps">
		<br>
		<div class="epps form form-horizontal">
			<?php
				foreach ($equipment as $key => $equip) {
					echo "<div class='form-group'>
					<label class='col-lg-4 control-label'>$equip</label>
					<div class='col-lg-4'>";
					echo $this->Form->input('EquipmentSize.'. $key .'.size', array(
						  				'div'=>false, 'label'=>false, 'class' => 'form-control'));
					echo "</div>
						</div>";

				}
			?>
	  		<div class='col-md-offset-5 col-md-3'>
	  			<a class='btn btn-primary btnPrevious'>Anterior</a>
	  			<a class='btn btn-primary btnNext'>Siguiente</a>
	  		</div>
	  		<br><br><br>
	  	</div>
  		
  	</div>
  	<!-- <div role="tabpanel" class="tab-pane fade" id="experience">
  		</br>
		<div class="experience form form-horizontal">
			<div class="form-group">
				<label class='col-lg-2 control-label'>company_id</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.company_id', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>position</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.position', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>date_in</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.date_in', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>date_out</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.date_out', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>out_reason</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.out_reason', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>boss</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.boss', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>number</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('WorkExperience.number', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
		</div>
  	
  		<div class="col-md-offset-5 col-md-3">
  			<a class="btn btn-primary btnPrevious">Anterior</a>
  			<a class="btn btn-primary btnNext">Siguiente</a>
  			
  		</div>
  	</div>-->
  	<div role="tabpanel" class="tab-pane fade" id="family">
  		</br>
		<div class="family form form-horizontal">
			<div class="form-group">
				<label class='col-lg-4 control-label'>Conyugue</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.type', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Nombres</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.name', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Apellidos</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.lastname', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Fecha de nacimiento</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.born_date', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Edad</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.age', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>DNI</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.dni', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Teléfono</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.phone', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Celular</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.celphone', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Grado de instrucción</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.instruction_grade', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Profesión</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.profession', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Ocupación</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.job', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Empresa</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.company_id', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Ocupación</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.company_job', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Domicilio de la empresa</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.occupation', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Teléfono</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.company_number', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Essalud</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.essalud', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-4 control-label'>Derecho habiente</label>
				<div class="col-lg-6">
					<?php echo $this->Form->input('Family.entitled', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="col-md-offset-5 col-md-3">
				<a class="btn btn-primary btnPrevious">Anterior</a>
				<a class="btn btn-primary btnNext">Siguiente</a>
				
			</div>
			<br><br><br>

		</div>
  		
  	</div>
  	<div role="tabpanel" class="tab-pane fade" id="home">
		<br>
		<div class="home form form-horizontal">
	  		<div class="form-group">
	  			<label class='col-lg-4 control-label'>Tendencia</label>
	  			<div class="col-lg-5">
	  				<?php 
	  				echo $this->Form->input('Home.tendency', array(
	  				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	  			</div>
	  		</div>
	  		<?php
	  			foreach ($services as $key => $equip) {
	  				echo "<div class='form-group'>
	  				<label class='col-lg-4 control-label'>$equip</label>
	  				<div class='col-lg-4'>";
	  				echo $this->Form->checkbox('HomesService.'. $key .'.id', array('hiddenField' => false,'div'=>false, 'label'=>false, 'class' => 'form-control'));
	  				echo "</div>
	  					</div>";

	  			}
	  		?>
	  	</div>

  	
  		<div class="col-md-offset-5 col-md-3">
  			<a class="btn btn-primary btnPrevious">Anterior</a>
  			<a class="btn btn-primary btnNext">Siguiente</a>
  			
  		</div>
  		<br><br><br>
  	</div>
  	<div role="tabpanel" class="tab-pane fade" id="health">
  		<br>
		<div class="health form form-horizontal">
			<div class="form-group">
				<label class='col-lg-2 control-label'>Sufrió accidente ó enfermedad de cuidado</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.accident', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Presenta alguna dolencia, dónde?</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.disease', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Tienen algún familiar con enfermedad grave</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.family', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Es alérgico(a)</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.allergy', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Grupo y factor sanguíneo</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.blood', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>En caso de emergencia llamar a quien</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.emergency_contact', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Teléfonos o celulares</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.emergency_number', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Dirección en caso de emergencia</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('MedicalRegister.emergency_adress', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="col-md-offset-5 col-md-3">
				<a class="btn btn-primary btnPrevious">Anterior</a>
				<a class="btn btn-primary btnNext">Siguiente</a>
				
			</div>
			<br><br><br>
		</div>
  		
  	</div>
  	<div role="tabpanel" class="tab-pane fade" id="social">
  		<br>
		<div class="social form form-horizontal">
			<div class="form-group">
				<label class='col-lg-2 control-label'>Qué expectativas tiene de su cargo en la empresa</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('SocialRegister.expectations', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Cómo describe usted su entorno familiar?</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('SocialRegister.familiar_surrounding', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Qué deporte practica?</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('SocialRegister.sport', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Pertenece a algún club social o deportivo	</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('SocialRegister.club', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>Cómo solucionaría un problema entre sus compañeros, explique?</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('SocialRegister.coworker_problem', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class='col-lg-2 control-label'>En que emplea su tiempo libre</label>
				<div class="col-lg-10">
					<?php echo $this->Form->input('SocialRegister.free_time', array(
					'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
				</div>
			</div>
		</div>
  	
  		<div class="col-md-offset-5 col-md-3">
  			<a class="btn btn-primary btnPrevious">Anterior</a>
  			<?php
  			$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
  			echo $this->Form->end($options);
  			?>
  		</div>
		<div class='form-actions'>
			<div class="col-md-offset-5 col-md-3">
				
			</div>
			<br><br><br><br>
		</div>
  	</div>
  </div>

<script>
	var number_studies = 1;

	$('.add_study').click(function(){
		var carrer = "<input name='data[Degree]["+ number_studies +"][career_id]' class='form-control' type='text' id='Degree"+ number_studies +"CareerId'>";
		var instruction = "<input name='data[InstructionGrade]["+ number_studies +"][name]' class='form-control' type='text' id='InstructionGrade"+ number_studies +"Name'>";
		var egress = "<input name='data[Degrees]["+ number_studies +"][egress_date]' class='form-control' type='text' id='Degrees"+ number_studies +"EgressDate'>";
		var institution = "<input name='data[Institution]["+ number_studies +"][name]' class='form-control' type='text' id='Institution"+ number_studies +"Name'>";
		var regime = "<input name='data[Institution]["+ number_studies +"][regime]' class='form-control' type='text' id='Institution"+ number_studies +"Regime'>";

		$('#studies_forms').append("\
			<div class='form-group'>\
			<label class='col-lg-2 control-label'>Carrera</label>\
			<div class='col-lg-10'>"+carrer+"</div></div>\
			<div class='form-group'>\
			<label class='col-lg-2 control-label'>Grado de Instrucción</label>\
			<div class='col-lg-10'>"+instruction+"</div></div>\
			<div class='form-group'>\
			<label class='col-lg-2 control-label'>Año de egreso</label>\
			<div class='col-lg-10'>"+egress+"</div></div>\
			<div class='form-group'>\
			<label class='col-lg-2 control-label'>Institución</label>\
			<div class='col-lg-10'>"+institution+"</div></div>\
			<div class='form-group'>\
			<label class='col-lg-2 control-label'>Régimen de la Institución</label>\
			<div class='col-lg-10'>"+regime+"</div></div>\
			");
		number_studies = number_studies + 1;
	});

	$('.btnNext').click(function(){
		$('.nav-tabs > .active').next('li').find('a').trigger('click');
	});

	$('.btnPrevious').click(function(){
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	});

	$('#myTabs a[href="#personal"]').tab('show');

	$("#WorkerBankId").select2({
		placeholder: "Selecciona un banco",
		language: "es",
		tags: true,
		single: true,
	});

	$("#WorkerDepartmentId").select2({
		placeholder: "Selecciona un Departamento",
		language: "es",
		tags: true,
		single: true,
	});

	$("#WorkerProvinceId").select2({
		placeholder: "Selecciona una Provincia",
		language: "es",
		tags: true,
		single: true,
	});

	$("#WorkerDistrictId").select2({
		placeholder: "Selecciona un Distrito",
		language: "es",
		tags: true,
		single: true,
	});

	$("#WorkerCivilStateId").select2({
		placeholder: "Selecciona un estado civíl",
		language: "es",
		tags: true,
		single: true,
	});

	$("#Degree0InstitutionId").select2({
		placeholder: "Selecciona un grado de institución",
		language: "es",
		tags: true,
		single: true,
	});

	$("#Degree0CareerId").select2({
		placeholder: "Selecciona una carrera",
		language: "es",
		tags: true,
		single: true,
	});

	$("#Degree0InstructionGradeId").select2({
		placeholder: "Selecciona un grado de instrucción",
		language: "es",
		tags: true,
		single: true,
	});


	var fecha_partida;
	var gasto_Ad;
	var fecha_llegada;
	var d = new Date();
	var year = d.getFullYear() - 18;
	d.setFullYear(year);

	/*$('#datetimepicker1').datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: year,
		minDate: "-100Y",
		dateFormat : 'yy/mm/dd',
		yearRange: '-100:' + year + '',
		defaultDate: d,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
		onSelect: function(dateText) {
			var dob = $("#datetimepicker1").val();
			var now = new Date();
			var birthdate = dob.split("/");
			var born = new Date(birthdate[0], birthdate[1]-1, birthdate[2]);
			age=get_age(born,now);
			var elem = document.getElementById("WorkerAge");
			elem.value = age;
		}
	});

	$('#datetimepicker2').datepicker({
		changeMonth: true,
		changeYear: true,
		minDate: year,
		maxDate: "-100Y",
		dateFormat : 'yy/mm/dd',
		yearRange: '100:' + year + '',
		defaultDate: d,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
	});*/

	function get_age(born, now) {
		var birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
		if (now >= birthday) 
			return now.getFullYear() - born.getFullYear();
		else
			return now.getFullYear() - born.getFullYear() - 1;
	}
</script>


