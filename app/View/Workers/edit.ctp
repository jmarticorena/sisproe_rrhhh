<div class="workers form form-horizontal">
<?php echo $this->Form->create('Worker'); ?>
	<fieldset>
		<legend><?php echo __('Editar Trabajador'); ?></legend>
			<div class="form-group">
	<!-- <label class='col-lg-2 control-label'>id</label> -->
	<div class="col-lg-10">
		<?php echo $this->Form->hidden('id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>DNI</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('dni', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Nombre</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('name', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Apellido</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('lastname', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Fecha de nacimiento</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('born_date', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Lugar de nacimiento</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('born_place', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Edad</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('age', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Teléfono</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('phone', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Celular</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('celphone', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Dirección</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('address', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Policlínico</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('polyclinic', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Policlínico-Otros</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('polyclinic_other', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>AFP</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('afp', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>CUSP</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('cusp', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>ONP</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('onp', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Brevete y Categoría</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('license_category', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Fecha de Caducidad</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('expiration_date', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>N° de Cuenta de Haberes</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('assets_account_number', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Banco</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('bank_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Distrito</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('district_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Estado Cívil</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('civil_state_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

