<div class="workers index">
	<h2><?php echo __('Trabajadores'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('dni', 'DNI'); ?></th>
			<th><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('lastname', 'Apellidos'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('born_date'); ?></th>
			<th><?php echo $this->Paginator->sort('born_place'); ?></th> -->
			<th><?php echo $this->Paginator->sort('age', 'Edad'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('phone'); ?></th> -->
			<th><?php echo $this->Paginator->sort('celphone', 'Celular'); ?></th>
			<th><?php echo $this->Paginator->sort('address', 'Dirección'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('polyclinic'); ?></th>
			<th><?php echo $this->Paginator->sort('polyclinic_other'); ?></th>
			<th><?php echo $this->Paginator->sort('afp'); ?></th>
			<th><?php echo $this->Paginator->sort('cusp'); ?></th>
			<th><?php echo $this->Paginator->sort('onp'); ?></th>
			<th><?php echo $this->Paginator->sort('license_category'); ?></th>
			<th><?php echo $this->Paginator->sort('expiration_date'); ?></th>
			<th><?php echo $this->Paginator->sort('assets_account_number'); ?></th>
			<th><?php echo $this->Paginator->sort('bank_id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('district_id', 'Distrito'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('civil_state_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($workers as $worker): ?>
	<tr>
		<!-- <td><?php echo h($worker['Worker']['id']); ?>&nbsp;</td> -->
		<td><?php echo h($worker['Worker']['dni']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['name']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['lastname']); ?>&nbsp;</td>
		<!-- <td><?php echo h($worker['Worker']['born_date']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['born_place']); ?>&nbsp;</td> -->
		<td><?php echo h($worker['Worker']['age']); ?>&nbsp;</td>
		<!-- <td><?php echo h($worker['Worker']['phone']); ?>&nbsp;</td> -->
		<td><?php echo h($worker['Worker']['celphone']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['address']); ?>&nbsp;</td>
		<!-- <td><?php echo h($worker['Worker']['polyclinic']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['polyclinic_other']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['afp']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['cusp']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['onp']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['license_category']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['expiration_date']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['assets_account_number']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($worker['Bank']['name'], array('controller' => 'banks', 'action' => 'view', $worker['Bank']['id'])); ?>
		</td> -->
		<td>
			<?php echo $this->Html->link($worker['District']['name'], array('controller' => 'districts', 'action' => 'view', $worker['District']['id'])); ?>
		</td>
		<!-- <td>
			<?php echo $this->Html->link($worker['CivilState']['name'], array('controller' => 'civil_states', 'action' => 'view', $worker['CivilState']['id'])); ?>
		</td>
		<td><?php echo h($worker['Worker']['created']); ?>&nbsp;</td>
		<td><?php echo h($worker['Worker']['updated']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $worker['Worker']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $worker['Worker']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $worker['Worker']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $worker['Worker']['name']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

