<div class="workExperiences view">
<h2><?php echo __('Experiencia Laboral'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Compañia'); ?></dt>
		<dd>
			<?php echo $this->Html->link($workExperience['Company']['name'], array('controller' => 'companies', 'action' => 'view', $workExperience['Company']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cargo'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['position']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha de ingreso'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['date_in']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha de egreso'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['date_out']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Razón de egreso'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['out_reason']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jefe'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['boss']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Número'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Trabajador'); ?></dt>
		<dd>
			<?php echo $this->Html->link($workExperience['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $workExperience['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($workExperience['WorkExperience']['updated']); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>

