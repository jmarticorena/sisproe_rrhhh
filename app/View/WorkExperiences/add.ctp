<div class="workExperiences form form-horizontal">
<?php echo $this->Form->create('WorkExperience'); ?>
<fieldset>
	<legend><?php echo __('Agregar Experiencia Laboral'); ?></legend>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Compañia</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('company_id', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Cargo</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('position', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Fecha de Ingreso</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('date_in', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Fecha de Egreso</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('date_out', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Razón de Egreso</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('out_reason', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Jefe</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('boss', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Número</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('number', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>Trabajador</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('worker_id', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

