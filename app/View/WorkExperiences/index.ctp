<div class="workExperiences index">
	<h2><?php echo __('Experiencias Laborales'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('position'); ?></th>
			<th><?php echo $this->Paginator->sort('date_in'); ?></th>
			<th><?php echo $this->Paginator->sort('date_out'); ?></th>
			<th><?php echo $this->Paginator->sort('out_reason'); ?></th>
			<th><?php echo $this->Paginator->sort('boss'); ?></th>
			<th><?php echo $this->Paginator->sort('number'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($workExperiences as $workExperience): ?>
	<tr>
		<!-- <td><?php echo h($workExperience['WorkExperience']['id']); ?>&nbsp;</td> -->
		<td>
			<?php echo $this->Html->link($workExperience['Company']['name'], array('controller' => 'companies', 'action' => 'view', $workExperience['Company']['id'])); ?>
		</td>
		<td><?php echo h($workExperience['WorkExperience']['position']); ?>&nbsp;</td>
		<td><?php echo h($workExperience['WorkExperience']['date_in']); ?>&nbsp;</td>
		<td><?php echo h($workExperience['WorkExperience']['date_out']); ?>&nbsp;</td>
		<td><?php echo h($workExperience['WorkExperience']['out_reason']); ?>&nbsp;</td>
		<td><?php echo h($workExperience['WorkExperience']['boss']); ?>&nbsp;</td>
		<td><?php echo h($workExperience['WorkExperience']['number']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($workExperience['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $workExperience['Worker']['id'])); ?>
		</td>
		<!-- <td><?php echo h($workExperience['WorkExperience']['created']); ?>&nbsp;</td>
		<td><?php echo h($workExperience['WorkExperience']['updated']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $workExperience['WorkExperience']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $workExperience['WorkExperience']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $workExperience['WorkExperience']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $workExperience['WorkExperience']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

