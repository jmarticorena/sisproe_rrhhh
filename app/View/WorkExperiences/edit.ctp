<div class="workExperiences form form-horizontal">
<?php echo $this->Form->create('WorkExperience'); ?>
	<fieldset>
		<legend><?php echo __('Edit Work Experience'); ?></legend>
			<div class="form-group">
	<!-- <label class='col-lg-2 control-label'>id</label> -->
	<div class="col-lg-10">
		<?php echo $this->Form->hidden('id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>company_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('company_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>position</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('position', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>date_in</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('date_in', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>date_out</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('date_out', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>out_reason</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('out_reason', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>boss</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('boss', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>number</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('number', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>worker_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

