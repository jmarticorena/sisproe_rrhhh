<div class="homesServices view">
<h2><?php echo __('Homes Service'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($homesService['HomesService']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Home Id'); ?></dt>
		<dd>
			<?php echo h($homesService['HomesService']['home_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Service Id'); ?></dt>
		<dd>
			<?php echo h($homesService['HomesService']['service_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($homesService['HomesService']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($homesService['HomesService']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

