<div class="col-sm-offset-4 col-sm-4 text-center alert alert-danger fade in" role="alert" style="z-index:1"> 
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
  <?php echo $message; ?>
</div>