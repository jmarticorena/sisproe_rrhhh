<div class="instructionGrades view">
<h2><?php echo __('Instruction Grade'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($instructionGrade['InstructionGrade']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($instructionGrade['InstructionGrade']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($instructionGrade['InstructionGrade']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($instructionGrade['InstructionGrade']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

