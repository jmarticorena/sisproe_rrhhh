<div class="careers index">
	<h2><?php echo __('Careers'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($careers as $career): ?>
	<tr>
		<td><?php echo h($career['Career']['id']); ?>&nbsp;</td>
		<td><?php echo h($career['Career']['name']); ?>&nbsp;</td>
		<td><?php echo h($career['Career']['created']); ?>&nbsp;</td>
		<td><?php echo h($career['Career']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $career['Career']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $career['Career']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $career['Career']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $career['Career']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

