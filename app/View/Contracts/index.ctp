<div class="contracts index">
	<h2><?php echo __('Contratos'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('Cargo'); ?></th>
			<th><?php echo $this->Paginator->sort('Inicio de Contrato'); ?></th>
			<th><?php echo $this->Paginator->sort('Fin de Contrato'); ?></th>
			<th><?php echo $this->Paginator->sort('Periodo de Prueba'); ?></th>
			<th><?php echo $this->Paginator->sort('Centro de Costo'); ?></th>
			<th><?php echo $this->Paginator->sort('Proyecto'); ?></th>
			<th><?php echo $this->Paginator->sort('Trabajador'); ?></th>
			<th><?php echo $this->Paginator->sort('Sueldo Mensual'); ?></th>
			<th><?php echo $this->Paginator->sort('Asignación Familiar'); ?></th>
			<th><?php echo $this->Paginator->sort('Vale Alimentos'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($contracts as $contract): ?>
	<tr>
		<!-- <td><?php echo h($contract['Contract']['id']); ?>&nbsp;</td> -->
		<td><?php echo h($contract['Contract']['position']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['contract_start']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['contract_end']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['trial_period']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($contract['CostCenter']['name'], array('controller' => 'cost_centers', 'action' => 'view', $contract['CostCenter']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($contract['Project']['name'], array('controller' => 'projects', 'action' => 'view', $contract['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($contract['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $contract['Worker']['id'])); ?>
		</td>
		<td><?php echo h($contract['Contract']['monthly']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['family']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['blood']); ?>&nbsp;</td>
		<!-- <td><?php echo h($contract['Contract']['created']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['updated']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $contract['Contract']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $contract['Contract']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $contract['Contract']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $contract['Worker']['name']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

