<div class="contracts view">
<h2><?php echo __('Contrato'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Cargo'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['position']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inicio de Contrato'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['contract_start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fin de Contrato'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['contract_end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Periodo de Prueba'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['trial_period']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Centro de Costo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contract['CostCenter']['name'], array('controller' => 'cost_centers', 'action' => 'view', $contract['CostCenter']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Proyecto'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contract['Project']['name'], array('controller' => 'projects', 'action' => 'view', $contract['Project']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Trabajador'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contract['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $contract['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sueldo Mensual'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['monthly']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Asignación Familiar'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['family']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vale Alimentos'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['blood']); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($contract['Contract']['updated']); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>

