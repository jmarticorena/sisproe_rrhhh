<div class="contracts index">
	<h2><?php echo __('Contratos - Reporte - Cesados'); ?></h2>
	<hr>
	<a href="report">
	<button type="button" class="btn btn-info">Todos</button>
	</a>
	<a href="report_1">
	<button type="button" class="btn btn-success">Activos</button>
	</a>

	<!-- Indicates caution should be taken with this action -->
	<a href="report_2">
	<button type="button" class="btn btn-warning">Por Cesar</button>
	</a>

	<!-- Indicates a dangerous or potentially negative action -->
	<a href="report_3">
	<button type="button" class="btn btn-danger">Cesados</button>
	</a>

	<a href="report_3.pdf">
	<button type="button" class="btn"><i class="fa fa-file-pdf-o"></i> PDF</button>
	</a>

	<hr>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('Cargo'); ?></th>
			<th><?php echo $this->Paginator->sort('Inicio de Contrato'); ?></th>
			<th><?php echo $this->Paginator->sort('Fin de Contrato'); ?></th>
			<th><?php echo $this->Paginator->sort('Tiempo Restante'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('Periodo de Prueba'); ?></th> -->
			<th><?php echo $this->Paginator->sort('Centro de Costo'); ?></th>
			<th><?php echo $this->Paginator->sort('Proyecto'); ?></th>
			<th><?php echo $this->Paginator->sort('Trabajador'); ?></th>
			<th><?php echo $this->Paginator->sort('Sueldo Mensual'); ?></th>
			<th><?php echo $this->Paginator->sort('Asignación Familiar'); ?></th>
			<th><?php echo $this->Paginator->sort('Vale Alimentos'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	$limit = 1; # 1 mes
	$green = '#449d44';
	$orange = '#f0ad4e';
	$red =  '#c9302c';
	?>
	<?php foreach ($contracts as $contract): ?>
		<!-- <td><?php echo h($contract['Contract']['id']); ?>&nbsp;</td> -->
		<?php 
		$actual = date('Y-m-d');

		$begin = $contract['Contract']['contract_start'];
		$end = $contract['Contract']['contract_end'];

		$diff = abs(strtotime($end) - strtotime($actual));

		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		if (strtotime($end) > strtotime($actual)) {
			# falta para vencerse
			$state = $orange; #falta poco para vencerse
			if ($months >= $limit){
				$state = $green; #falta mucho para vencerse
			}
		}
		else{
			# se vencio
			$state = $red;
		}

		?>
	<?php echo "<tr>"; ?>
		<td><font color="#000"><?php echo h($contract['Contract']['position']); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($begin); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($end); ?>&nbsp;</font></td>
		<td><font color="#000"><?php printf("%d años, %d meses, %d días", $years, $months, $days); ?>&nbsp;</td>
		<!-- <td><?php echo h($contract['Contract']['trial_period']); ?>&nbsp;</td> -->
		<td><font color="#000">
			<?php echo h($contract['CostCenter']['name']); ?>
		</font></td>
		<td><font color="#000">
			<?php echo h($contract['Project']['name']); ?>
		</font></td>
		<td><font color="#000">
			<?php echo $this->Html->link($contract['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $contract['Worker']['id'])); ?>
		</font></td>
		<td><font color="#000"><?php echo h($contract['Contract']['monthly']); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($contract['Contract']['family']); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($contract['Contract']['blood']); ?>&nbsp;</font></td>
		<!-- <td><?php echo h($contract['Contract']['created']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['updated']); ?>&nbsp;</td> -->
		<td class="actions"><font color="#000">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $contract['Contract']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $contract['Contract']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $contract['Contract']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $contract['Worker']['name']))); ?>
		</font></td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

