<div class="contracts form form-horizontal">
<?php echo $this->Form->create('Contract'); ?>
	<fieldset>
		<legend><?php echo __('Editar Contrato'); ?></legend>
			<div class="form-group">
	<!-- <label class='col-lg-2 control-label'>id</label> -->
	<div class="col-lg-10">
		<?php echo $this->Form->hidden('id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Cargo</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('position', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Inicio de Contrato</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('contract_start', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Fin de Contrato</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('contract_end', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Periodo de Prueba</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('trial_period', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Centro de Costo</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('cost_center_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Proyecto</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('project_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Trabajador</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Sueldo Mensual</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('monthly', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Asignación Familiar</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('family', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Vale Alimentos</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('blood', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

