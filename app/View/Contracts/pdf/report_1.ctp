<div class="contracts index">
	<h2><?php echo __('Contratos - Reporte - Activos'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo h('id'); ?></th> -->
			<th><?php echo h('Cargo'); ?></th>
			<th><?php echo h('Inicio de Contrato'); ?></th>
			<th><?php echo h('Fin de Contrato'); ?></th>
			<th><?php echo h('Tiempo Restante'); ?></th>
			<!-- <th><?php echo h('Periodo de Prueba'); ?></th> -->
			<th><?php echo h('Centro de Costo'); ?></th>
			<th><?php echo h('Proyecto'); ?></th>
			<th><?php echo h('Trabajador'); ?></th>
			<th><?php echo h('Sueldo Mensual'); ?></th>
			<th><?php echo h('Asignación Familiar'); ?></th>
			<th><?php echo h('Vale Alimentos'); ?></th>
			<!-- <th><?php echo h('created'); ?></th>
			<th><?php echo h('updated'); ?></th> -->
	</tr>
	</thead>
	<tbody>
	<?php
	$limit = 1; # 1 mes
	$green = '#449d44';
	$orange = '#f0ad4e';
	$red =  '#c9302c';
	?>
	<?php foreach ($contracts as $contract): ?>
		<!-- <td><?php echo h($contract['Contract']['id']); ?>&nbsp;</td> -->
		<?php 
		$actual = date('Y-m-d');

		$begin = $contract['Contract']['contract_start'];
		$end = $contract['Contract']['contract_end'];

		$diff = abs(strtotime($end) - strtotime($actual));

		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		if (strtotime($end) > strtotime($actual)) {
			# falta para vencerse
			$state = $orange; #falta poco para vencerse
			if ($months >= $limit){
				$state = $green; #falta mucho para vencerse
			}
		}
		else{
			# se vencio
			$state = $red;
		}

		?>
	<?php echo "<tr>"; ?>
		<td><font color="#000"><?php echo h($contract['Contract']['position']); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($begin); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($end); ?>&nbsp;</font></td>
		<td><font color="#000"><?php printf("%d años, %d meses, %d días", $years, $months, $days); ?>&nbsp;</td>
		<!-- <td><?php echo h($contract['Contract']['trial_period']); ?>&nbsp;</td> -->
		<td><font color="#000">
			<?php echo h($contract['CostCenter']['name']); ?>
		</font></td>
		<td><font color="#000">
			<?php echo h($contract['Project']['name']); ?>
		</font></td>
		<td><font color="#000">
			<?php echo h($contract['Worker']['name']); ?>
		</font></td>
		<td><font color="#000"><?php echo h($contract['Contract']['monthly']); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($contract['Contract']['family']); ?>&nbsp;</font></td>
		<td><font color="#000"><?php echo h($contract['Contract']['blood']); ?>&nbsp;</font></td>
		<!-- <td><?php echo h($contract['Contract']['created']); ?>&nbsp;</td>
		<td><?php echo h($contract['Contract']['updated']); ?>&nbsp;</td> -->
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">
</div>
</div>

