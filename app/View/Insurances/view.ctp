<div class="insurances view">
<h2><?php echo __('Seguro'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($insurance['Insurance']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Válido del'); ?></dt>
		<dd>
			<?php echo h($insurance['Insurance']['begin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hasta'); ?></dt>
		<dd>
			<?php echo h($insurance['Insurance']['end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Trabajador'); ?></dt>
		<dd>
			<?php echo $this->Html->link($insurance['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $insurance['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($insurance['Insurance']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($insurance['Insurance']['updated']); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>

