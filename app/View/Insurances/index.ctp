<div class="insurances index">
	<h2><?php echo __('Seguros'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('begin', 'Válido del'); ?></th>
			<th><?php echo $this->Paginator->sort('end', 'Hasta'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id', 'Trabajador'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($insurances as $insurance): ?>
	<tr>
		<!-- <td><?php echo h($insurance['Insurance']['id']); ?>&nbsp;</td> -->
		<td><?php echo h($insurance['Insurance']['begin']); ?>&nbsp;</td>
		<td><?php echo h($insurance['Insurance']['end']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($insurance['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $insurance['Worker']['id'])); ?>
		</td>
		<!-- <td><?php echo h($insurance['Insurance']['created']); ?>&nbsp;</td>
		<td><?php echo h($insurance['Insurance']['updated']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $insurance['Insurance']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $insurance['Insurance']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $insurance['Insurance']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $insurance['Worker']['name']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

