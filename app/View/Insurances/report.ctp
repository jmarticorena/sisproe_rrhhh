<div class="insurances index">
	<h2><?php echo __('Seguros - Reporte'); ?></h2>
	<hr>
	<a href="report">
	<button type="button" class="btn btn-info">Todos</button>
	</a>
	<a href="report_1">
	<button type="button" class="btn btn-success">Activos</button>
	</a>

	<!-- Indicates caution should be taken with this action -->
	<a href="report_2">
	<button type="button" class="btn btn-warning">Por Vencer</button>
	</a>

	<!-- Indicates a dangerous or potentially negative action -->
	<a href="report_3">
	<button type="button" class="btn btn-danger">Vencidos</button>
	</a>

	<a href="report.pdf">
	<button type="button" class="btn"><i class="fa fa-file-pdf-o"></i> PDF</button>
	</a>

	<hr>
	<table class="table table-bordered">
	<thead>
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('begin', 'Válido del'); ?></th>
			<th><?php echo $this->Paginator->sort('end', 'Hasta'); ?></th>
			<th><?php echo $this->Paginator->sort('Tiempo Restante'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id', 'Trabajador'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th> -->
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	$limit = 1; # 1 mes
	$green = '#449d44';
	$orange = '#f0ad4e';
	$red =  '#c9302c';
	?>
	<?php foreach ($insurances as $insurance): ?>
		<?php 
		$actual = date('Y-m-d');

		$begin = $insurance['Insurance']['begin'];
		$end = $insurance['Insurance']['end'];

		$diff = abs(strtotime($end) - strtotime($actual));

		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		if (strtotime($end) > strtotime($actual)) {
			# falta para vencerse
			$state = $orange; #falta poco para vencerse
			if ($months >= $limit){
				$state = $green; #falta mucho para vencerse
			}
		}
		else{
			# se vencio
			$state = $red;
		}

		?>
	<?php echo "<tr bgcolor='$state'>"; ?>
		<!-- <td><?php echo h($insurance['Insurance']['id']); ?>&nbsp;</td> -->
		<td><font color="#fff"><?php echo h($begin); ?>&nbsp;</font></td>
		<td><font color="#fff"><?php echo h($end); ?>&nbsp;</font></td>
		<td><font color="#fff"><?php printf("%d años, %d meses, %d días", $years, $months, $days); ?>&nbsp;</td>
		<td><font color="#fff">
			<?php echo h($insurance['Worker']['name']); ?>
		</font></td>
		<!-- <td><?php echo h($insurance['Insurance']['created']); ?>&nbsp;</td>
		<td><?php echo h($insurance['Insurance']['updated']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $insurance['Insurance']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $insurance['Insurance']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $insurance['Insurance']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $insurance['Worker']['name']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

