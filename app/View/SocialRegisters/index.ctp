<div class="socialRegisters index">
	<h2><?php echo __('Social Registers'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('expectations'); ?></th>
			<th><?php echo $this->Paginator->sort('familiar_surrounding'); ?></th>
			<th><?php echo $this->Paginator->sort('sport'); ?></th>
			<th><?php echo $this->Paginator->sort('club'); ?></th>
			<th><?php echo $this->Paginator->sort('coworker_problem'); ?></th>
			<th><?php echo $this->Paginator->sort('free_time'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($socialRegisters as $socialRegister): ?>
	<tr>
		<td><?php echo h($socialRegister['SocialRegister']['id']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['expectations']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['familiar_surrounding']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['sport']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['club']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['coworker_problem']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['free_time']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($socialRegister['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $socialRegister['Worker']['id'])); ?>
		</td>
		<td><?php echo h($socialRegister['SocialRegister']['created']); ?>&nbsp;</td>
		<td><?php echo h($socialRegister['SocialRegister']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $socialRegister['SocialRegister']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $socialRegister['SocialRegister']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $socialRegister['SocialRegister']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $socialRegister['SocialRegister']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

