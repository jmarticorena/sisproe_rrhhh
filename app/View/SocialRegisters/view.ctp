<div class="socialRegisters view">
<h2><?php echo __('Social Register'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expectations'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['expectations']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Familiar Surrounding'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['familiar_surrounding']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sport'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['sport']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Club'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['club']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Coworker Problem'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['coworker_problem']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Free Time'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['free_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($socialRegister['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $socialRegister['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($socialRegister['SocialRegister']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

