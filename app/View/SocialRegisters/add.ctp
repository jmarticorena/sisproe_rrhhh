<div class="socialRegisters form form-horizontal">
<?php echo $this->Form->create('SocialRegister'); ?>
	<fieldset>
		<legend><?php echo __('Add Social Register'); ?></legend>
		<div class="form-group">
			<label class='col-lg-2 control-label'>expectations</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('expectations', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class='col-lg-2 control-label'>familiar_surrounding</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('familiar_surrounding', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class='col-lg-2 control-label'>sport</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('sport', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class='col-lg-2 control-label'>club</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('club', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class='col-lg-2 control-label'>coworker_problem</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('coworker_problem', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class='col-lg-2 control-label'>free_time</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('free_time', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<label class='col-lg-2 control-label'>worker_id</label>
			<div class="col-lg-10">
				<?php echo $this->Form->input('worker_id', array(
				'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

