<div class="equipmentSizes view">
<h2><?php echo __('Equipment Size'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($equipmentSize['EquipmentSize']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Size'); ?></dt>
		<dd>
			<?php echo h($equipmentSize['EquipmentSize']['size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($equipmentSize['Equipment']['name'], array('controller' => 'equipment', 'action' => 'view', $equipmentSize['Equipment']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($equipmentSize['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $equipmentSize['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($equipmentSize['EquipmentSize']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($equipmentSize['EquipmentSize']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

