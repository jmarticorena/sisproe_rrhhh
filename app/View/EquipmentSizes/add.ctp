<div class="equipmentSizes form form-horizontal">
<?php echo $this->Form->create('EquipmentSize'); ?>
<fieldset>
	<legend><?php echo __('Add Equipment Size'); ?></legend>
	<div class="form-group">
		<label class='col-lg-2 control-label'>size</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('size', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>equipment_id</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('equipment_id', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class='col-lg-2 control-label'>worker_id</label>
		<div class="col-lg-10">
			<?php echo $this->Form->input('worker_id', array(
			'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

