<div class="equipmentSizes index">
	<h2><?php echo __('Equipment Sizes'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('size'); ?></th>
			<th><?php echo $this->Paginator->sort('equipment_id'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($equipmentSizes as $equipmentSize): ?>
	<tr>
		<td><?php echo h($equipmentSize['EquipmentSize']['id']); ?>&nbsp;</td>
		<td><?php echo h($equipmentSize['EquipmentSize']['size']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($equipmentSize['Equipment']['name'], array('controller' => 'equipment', 'action' => 'view', $equipmentSize['Equipment']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($equipmentSize['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $equipmentSize['Worker']['id'])); ?>
		</td>
		<td><?php echo h($equipmentSize['EquipmentSize']['created']); ?>&nbsp;</td>
		<td><?php echo h($equipmentSize['EquipmentSize']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $equipmentSize['EquipmentSize']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $equipmentSize['EquipmentSize']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $equipmentSize['EquipmentSize']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $equipmentSize['EquipmentSize']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

