<div class="legacies form form-horizontal">
<?php echo $this->Form->create('Legacy'); ?>
	<fieldset>
		<legend><?php echo __('Agregar hijo'); ?></legend>
			<div class="form-group">
	<label class='col-lg-2 control-label'>Nombres</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('name', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Apellidos</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('lastname', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Fecha de nacimiento</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('born_date', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Grado de Instruccion</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('instruction_grade', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>ESSALUD</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('essalud', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>Conyugue</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('family_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

