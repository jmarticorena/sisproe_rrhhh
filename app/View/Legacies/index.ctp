<div class="legacies index">
	<h2><?php echo __('Legacies'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('lastname'); ?></th>
			<th><?php echo $this->Paginator->sort('born_date'); ?></th>
			<th><?php echo $this->Paginator->sort('instruction_grade'); ?></th>
			<th><?php echo $this->Paginator->sort('essalud'); ?></th>
			<th><?php echo $this->Paginator->sort('family_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($legacies as $legacy): ?>
	<tr>
		<td><?php echo h($legacy['Legacy']['id']); ?>&nbsp;</td>
		<td><?php echo h($legacy['Legacy']['name']); ?>&nbsp;</td>
		<td><?php echo h($legacy['Legacy']['lastname']); ?>&nbsp;</td>
		<td><?php echo h($legacy['Legacy']['born_date']); ?>&nbsp;</td>
		<td><?php echo h($legacy['Legacy']['instruction_grade']); ?>&nbsp;</td>
		<td><?php echo h($legacy['Legacy']['essalud']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($legacy['Family']['name'], array('controller' => 'families', 'action' => 'view', $legacy['Family']['id'])); ?>
		</td>
		<td><?php echo h($legacy['Legacy']['created']); ?>&nbsp;</td>
		<td><?php echo h($legacy['Legacy']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $legacy['Legacy']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $legacy['Legacy']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $legacy['Legacy']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $legacy['Legacy']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

