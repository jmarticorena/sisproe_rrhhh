<div class="legacies view">
<h2><?php echo __('Legacy'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lastname'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['lastname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Born Date'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['born_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Instruction Grade'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['instruction_grade']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Essalud'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['essalud']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Family'); ?></dt>
		<dd>
			<?php echo $this->Html->link($legacy['Family']['name'], array('controller' => 'families', 'action' => 'view', $legacy['Family']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($legacy['Legacy']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

