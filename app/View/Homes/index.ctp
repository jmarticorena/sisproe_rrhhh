<div class="homes index">
	<h2><?php echo __('Homes'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('tendency'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($homes as $home): ?>
	<tr>
		<td><?php echo h($home['Home']['id']); ?>&nbsp;</td>
		<td><?php echo h($home['Home']['tendency']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($home['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $home['Worker']['id'])); ?>
		</td>
		<td><?php echo h($home['Home']['created']); ?>&nbsp;</td>
		<td><?php echo h($home['Home']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $home['Home']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $home['Home']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $home['Home']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $home['Home']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

