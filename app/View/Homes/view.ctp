<div class="homes view">
<h2><?php echo __('Home'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($home['Home']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tendency'); ?></dt>
		<dd>
			<?php echo h($home['Home']['tendency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($home['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $home['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($home['Home']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($home['Home']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

