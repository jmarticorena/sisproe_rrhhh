<div class="lenguageLevels form form-horizontal">
<?php echo $this->Form->create('LenguageLevel'); ?>
	<fieldset>
		<legend><?php echo __('Add Lenguage Level'); ?></legend>
			<div class="form-group">
	<label class='col-lg-2 control-label'>speak</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('speak', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>write</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('write', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>understand</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('understand', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>level</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('level', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>language_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('language_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
		<div class="form-group">
	<label class='col-lg-2 control-label'>worker_id</label>
	<div class="col-lg-10">
		<?php echo $this->Form->input('worker_id', array(
		'div'=>false, 'label'=>false, 'class' => 'form-control')); ?>
	</div>
</div>
	</fieldset>
<div class='form-actions'>
	<div class="col-md-offset-4 col-md-3">
		<?php
		$options = array('label' => 'Guardar', 'class' => 'btn btn-success', 'div' => false);
		echo $this->Form->end($options);
		?>
	</div>
	<div class="col-md-3">
		<?php
		echo $this->Form->button('Cancelar', array(
			'type' => 'button', 'class' => 'btn btn-danger', 'div' => false,
			'onclick' => 'history.back();'
			));
		?>
	</div>
	<br>
	<br>
</div>
</div>

