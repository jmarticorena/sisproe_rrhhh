<div class="lenguageLevels index">
	<h2><?php echo __('Lenguage Levels'); ?></h2>
	<table class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('speak'); ?></th>
			<th><?php echo $this->Paginator->sort('write'); ?></th>
			<th><?php echo $this->Paginator->sort('understand'); ?></th>
			<th><?php echo $this->Paginator->sort('level'); ?></th>
			<th><?php echo $this->Paginator->sort('language_id'); ?></th>
			<th><?php echo $this->Paginator->sort('worker_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($lenguageLevels as $lenguageLevel): ?>
	<tr>
		<td><?php echo h($lenguageLevel['LenguageLevel']['id']); ?>&nbsp;</td>
		<td><?php echo h($lenguageLevel['LenguageLevel']['speak']); ?>&nbsp;</td>
		<td><?php echo h($lenguageLevel['LenguageLevel']['write']); ?>&nbsp;</td>
		<td><?php echo h($lenguageLevel['LenguageLevel']['understand']); ?>&nbsp;</td>
		<td><?php echo h($lenguageLevel['LenguageLevel']['level']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($lenguageLevel['Language']['name'], array('controller' => 'languages', 'action' => 'view', $lenguageLevel['Language']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($lenguageLevel['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $lenguageLevel['Worker']['id'])); ?>
		</td>
		<td><?php echo h($lenguageLevel['LenguageLevel']['created']); ?>&nbsp;</td>
		<td><?php echo h($lenguageLevel['LenguageLevel']['updated']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $lenguageLevel['LenguageLevel']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $lenguageLevel['LenguageLevel']['id'])); ?>
			<?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $lenguageLevel['LenguageLevel']['id']), array('confirm' => __('Estas seguro de borrar a %s?', $lenguageLevel['LenguageLevel']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		<div class="well well-small well-shadow text-center">

  <strong>
    <?php
    echo $this->Paginator->counter(array(
      'format' => __('Pág. {:page} de {:pages}, mostrando {:current} registros de {:count} en total.')
    ));
    ?>
    
    <br>
    <?php
    echo $this->Paginator->prev('< ' . __('Anterior '), array(), null, array('class' => 'prev disabled', 'escape' => false));
    echo $this->Paginator->numbers(array('separator' => ' | '));
    echo $this->Paginator->next(__(' Siguiente') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
  </strong>
  
</div>
</div>

