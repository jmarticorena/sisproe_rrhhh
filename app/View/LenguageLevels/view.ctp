<div class="lenguageLevels view">
<h2><?php echo __('Lenguage Level'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Speak'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['speak']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Write'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['write']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Understand'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['understand']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['level']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Language'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lenguageLevel['Language']['name'], array('controller' => 'languages', 'action' => 'view', $lenguageLevel['Language']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Worker'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lenguageLevel['Worker']['name'], array('controller' => 'workers', 'action' => 'view', $lenguageLevel['Worker']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Updated'); ?></dt>
		<dd>
			<?php echo h($lenguageLevel['LenguageLevel']['updated']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

