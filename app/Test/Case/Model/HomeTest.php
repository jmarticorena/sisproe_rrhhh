<?php
App::uses('Home', 'Model');

/**
 * Home Test Case
 *
 */
class HomeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.home',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.lenguage_level',
		'app.language',
		'app.medical_register',
		'app.social_register',
		'app.work_experience',
		'app.company',
		'app.service',
		'app.homes_service'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Home = ClassRegistry::init('Home');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Home);

		parent::tearDown();
	}

}
