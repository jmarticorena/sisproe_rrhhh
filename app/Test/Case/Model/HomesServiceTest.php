<?php
App::uses('HomesService', 'Model');

/**
 * HomesService Test Case
 *
 */
class HomesServiceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.homes_service',
		'app.homes',
		'app.services'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->HomesService = ClassRegistry::init('HomesService');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->HomesService);

		parent::tearDown();
	}

}
