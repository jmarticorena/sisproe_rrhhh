<?php
App::uses('InstructionGrade', 'Model');

/**
 * InstructionGrade Test Case
 *
 */
class InstructionGradeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.instruction_grade',
		'app.degree',
		'app.institution',
		'app.careers',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.home',
		'app.lenguage_level',
		'app.language',
		'app.medical_register',
		'app.social_register',
		'app.work_experience',
		'app.company'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->InstructionGrade = ClassRegistry::init('InstructionGrade');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InstructionGrade);

		parent::tearDown();
	}

}
