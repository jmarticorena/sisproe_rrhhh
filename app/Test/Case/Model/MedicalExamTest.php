<?php
App::uses('MedicalExam', 'Model');

/**
 * MedicalExam Test Case
 *
 */
class MedicalExamTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.medical_exam',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.career',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.company',
		'app.work_experience',
		'app.legacy',
		'app.home',
		'app.service',
		'app.homes_service',
		'app.lenguage_level',
		'app.language',
		'app.medical_register',
		'app.social_register'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MedicalExam = ClassRegistry::init('MedicalExam');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MedicalExam);

		parent::tearDown();
	}

}
