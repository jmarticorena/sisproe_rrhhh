<?php
App::uses('SocialRegister', 'Model');

/**
 * SocialRegister Test Case
 *
 */
class SocialRegisterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.social_register',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.home',
		'app.lenguage_level',
		'app.medical_register',
		'app.work_experience',
		'app.company'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SocialRegister = ClassRegistry::init('SocialRegister');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SocialRegister);

		parent::tearDown();
	}

}
