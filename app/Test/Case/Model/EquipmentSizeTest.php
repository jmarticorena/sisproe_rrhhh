<?php
App::uses('EquipmentSize', 'Model');

/**
 * EquipmentSize Test Case
 *
 */
class EquipmentSizeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.equipment_size',
		'app.equipment',
		'app.worker'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EquipmentSize = ClassRegistry::init('EquipmentSize');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EquipmentSize);

		parent::tearDown();
	}

}
