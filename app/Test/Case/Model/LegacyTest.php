<?php
App::uses('Legacy', 'Model');

/**
 * Legacy Test Case
 *
 */
class LegacyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.legacy',
		'app.family'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Legacy = ClassRegistry::init('Legacy');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Legacy);

		parent::tearDown();
	}

}
