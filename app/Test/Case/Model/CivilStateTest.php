<?php
App::uses('CivilState', 'Model');

/**
 * CivilState Test Case
 *
 */
class CivilStateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.civil_state',
		'app.worker'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CivilState = ClassRegistry::init('CivilState');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CivilState);

		parent::tearDown();
	}

}
