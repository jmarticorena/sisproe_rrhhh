<?php
App::uses('Insurance', 'Model');

/**
 * Insurance Test Case
 *
 */
class InsuranceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.insurance',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.career',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.company',
		'app.work_experience',
		'app.legacy',
		'app.home',
		'app.service',
		'app.homes_service',
		'app.lenguage_level',
		'app.language',
		'app.medical_register',
		'app.social_register'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Insurance = ClassRegistry::init('Insurance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Insurance);

		parent::tearDown();
	}

}
