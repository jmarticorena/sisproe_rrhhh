<?php
App::uses('Institution', 'Model');

/**
 * Institution Test Case
 *
 */
class InstitutionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.institution',
		'app.degree',
		'app.instruction_grade',
		'app.careers',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.home',
		'app.lenguage_level',
		'app.language',
		'app.medical_register',
		'app.social_register',
		'app.work_experience',
		'app.company'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Institution = ClassRegistry::init('Institution');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Institution);

		parent::tearDown();
	}

}
