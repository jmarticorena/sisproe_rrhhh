<?php
App::uses('Province', 'Model');

/**
 * Province Test Case
 *
 */
class ProvinceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.province',
		'app.department',
		'app.district',
		'app.worker',
		'app.bank',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.home',
		'app.lenguage_level',
		'app.medical_register',
		'app.social_register',
		'app.work_experience',
		'app.company'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Province = ClassRegistry::init('Province');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Province);

		parent::tearDown();
	}

}
