<?php
App::uses('LenguageLevel', 'Model');

/**
 * LenguageLevel Test Case
 *
 */
class LenguageLevelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.lenguage_level',
		'app.language',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.home',
		'app.medical_register',
		'app.social_register',
		'app.work_experience',
		'app.company'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LenguageLevel = ClassRegistry::init('LenguageLevel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LenguageLevel);

		parent::tearDown();
	}

}
