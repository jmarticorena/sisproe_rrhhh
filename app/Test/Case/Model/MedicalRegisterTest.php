<?php
App::uses('MedicalRegister', 'Model');

/**
 * MedicalRegister Test Case
 *
 */
class MedicalRegisterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.medical_register',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.department',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.company',
		'app.work_experience',
		'app.legacy',
		'app.home',
		'app.service',
		'app.homes_service',
		'app.lenguage_level',
		'app.language',
		'app.social_register'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MedicalRegister = ClassRegistry::init('MedicalRegister');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MedicalRegister);

		parent::tearDown();
	}

}
