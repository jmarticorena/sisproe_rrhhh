<?php
App::uses('Worker', 'Model');

/**
 * Worker Test Case
 *
 */
class WorkerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.family',
		'app.home',
		'app.lenguage_level',
		'app.medical_register',
		'app.social_register',
		'app.work_experience'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Worker = ClassRegistry::init('Worker');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Worker);

		parent::tearDown();
	}

}
