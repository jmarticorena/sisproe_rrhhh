<?php
App::uses('WorkExperience', 'Model');

/**
 * WorkExperience Test Case
 *
 */
class WorkExperienceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work_experience',
		'app.company',
		'app.family',
		'app.worker',
		'app.bank',
		'app.district',
		'app.province',
		'app.civil_state',
		'app.degree',
		'app.instruction_grade',
		'app.institution',
		'app.careers',
		'app.equipment_size',
		'app.equipment',
		'app.home',
		'app.lenguage_level',
		'app.medical_register',
		'app.social_register'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WorkExperience = ClassRegistry::init('WorkExperience');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WorkExperience);

		parent::tearDown();
	}

}
