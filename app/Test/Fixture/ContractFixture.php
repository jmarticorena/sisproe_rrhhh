<?php
/**
 * ContractFixture
 *
 */
class ContractFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'position' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'contract_start' => array('type' => 'date', 'null' => false, 'default' => null),
		'contract_end' => array('type' => 'date', 'null' => false, 'default' => null),
		'trial_period' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'cost_center_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'project_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'worker_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'monthly' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'family' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'blood' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'updated' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => array('id', 'cost_center_id', 'project_id', 'worker_id'), 'unique' => 1),
			'fk_workers_center_costs_centers_costs1_idx' => array('column' => 'cost_center_id', 'unique' => 0),
			'fk_workers_center_costs_projects1_idx' => array('column' => 'project_id', 'unique' => 0),
			'fk_workers_center_costs_workers1_idx' => array('column' => 'worker_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'position' => 'Lorem ipsum dolor sit amet',
			'contract_start' => '2015-07-22',
			'contract_end' => '2015-07-22',
			'trial_period' => 'Lorem ipsum dolor sit amet',
			'cost_center_id' => 1,
			'project_id' => 1,
			'worker_id' => 1,
			'monthly' => 1,
			'family' => 1,
			'blood' => 1,
			'created' => '2015-07-22 16:07:16',
			'updated' => '2015-07-22 16:07:16'
		),
	);

}
