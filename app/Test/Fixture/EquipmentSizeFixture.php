<?php
/**
 * EquipmentSizeFixture
 *
 */
class EquipmentSizeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'size' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'equipment_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'worker_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('id', 'equipment_id', 'worker_id'), 'unique' => 1),
			'fk_equipment_sizes_equipments1_idx' => array('column' => 'equipment_id', 'unique' => 0),
			'fk_equipment_sizes_workers1_idx' => array('column' => 'worker_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'size' => 'Lorem ipsum dolor sit amet',
			'equipment_id' => 1,
			'worker_id' => 1
		),
	);

}
