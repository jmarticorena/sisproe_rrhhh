<?php
/**
 * DegreeFixture
 *
 */
class DegreeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'instruction_grade_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'institution_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'careers_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'worker_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('id', 'instruction_grade_id', 'institution_id', 'careers_id', 'worker_id'), 'unique' => 1),
			'fk_degrees_instruction_grades1_idx' => array('column' => 'instruction_grade_id', 'unique' => 0),
			'fk_degrees_institutions1_idx' => array('column' => 'institution_id', 'unique' => 0),
			'fk_degrees_careers1_idx' => array('column' => 'careers_id', 'unique' => 0),
			'fk_degrees_workers1_idx' => array('column' => 'worker_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'instruction_grade_id' => 1,
			'institution_id' => 1,
			'careers_id' => 1,
			'worker_id' => 1
		),
	);

}
