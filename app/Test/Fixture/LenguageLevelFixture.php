<?php
/**
 * LenguageLevelFixture
 *
 */
class LenguageLevelFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'speak' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'write' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'understand' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'level' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'language_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'worker_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('id', 'language_id', 'worker_id'), 'unique' => 1),
			'fk_lenguage_levels_languages1_idx' => array('column' => 'language_id', 'unique' => 0),
			'fk_lenguage_levels_workers1_idx' => array('column' => 'worker_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'speak' => 1,
			'write' => 1,
			'understand' => 1,
			'level' => 1,
			'language_id' => 1,
			'worker_id' => 1
		),
	);

}
