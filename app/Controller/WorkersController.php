<?php
App::uses('AppController', 'Controller');
/**
 * Workers Controller
 *
 * @property Worker $Worker
 * @property PaginatorComponent $Paginator
 */
class WorkersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Worker->recursive = 0;
		$this->set('workers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Worker->exists($id)) {
			throw new NotFoundException(__('Invalid worker'));
		}
		$options = array('conditions' => array('Worker.' . $this->Worker->primaryKey => $id));
		$this->set('worker', $this->Worker->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('Province');
		$this->loadModel('Department');
		$this->loadModel('District');
		$this->loadModel('CivilState');
		$this->loadModel('Bank');
		$this->loadModel('Career');
		$this->loadModel('InstructionGrade');
		$this->loadModel('Institution');
		$this->loadModel('Degrees');
		$this->loadModel('Equipment');
		$this->loadModel('EquipmentSize');
		$this->loadModel('Service');
		$this->loadModel('HomesService');
		$this->loadModel('Home');
		$this->loadModel('Company');
		$this->loadModel('Family');
		

		if ($this->request->is('post')) {
			$this->Worker->create();

			$department = $this->Department->find('first', array(
				'conditions' => array('Department.name' => $this->request->data['Worker']['department_id'])
				));
			if ($department){
				$this->request->data['Worker']['department_id'] = $department['Department']['id'];
				$this->Department->id = $department['Department']['id'];
			}
			else{
				$department = array(
					'Department' => array(
						'name' => $this->request->data['Worker']['department_id']
						)
					);
				$this->Department->create();
				$this->Department->save($department);
				$this->request->data['Worker']['department_id'] = $this->Department->id;
			}


			$province = $this->Province->find('first', array(
				'conditions' => array('Province.name' => $this->request->data['Worker']['province_id'])
				));
			if ($province){
				$this->request->data['Worker']['province_id'] = $province['Province']['id'];
				$this->Province->id = $province['Province']['id'];
			}
			else{
				$province = array(
					'Province' => array(
						'name' => $this->request->data['Worker']['province_id'],
						'department_id' => $this->Department->id
						)
					);
				$this->Province->create();
				$this->Province->save($province);
				$this->request->data['Worker']['province_id'] = $this->Province->id;
			}

			$district = $this->District->find('first', array(
				'conditions' => array('District.name' => $this->request->data['Worker']['district_id'])
				));
			if ($district){
				$this->request->data['Worker']['district_id'] = $district['District']['id'];
				$this->District->id = $district['District']['id'];
			}
			else{
				$district = array(
					'District' => array(
						'name' => $this->request->data['Worker']['district_id'],
						'province_id' => $this->Province->id
						)
					);
				$this->District->create();
				$this->District->save($district);
				$this->request->data['Worker']['district_id'] = $this->District->id;
			}


			$civil_state = $this->CivilState->find('first', array(
				'conditions' => array('CivilState.name' => $this->request->data['Worker']['civil_state_id'])
				));
			if ($civil_state){
				$this->request->data['Worker']['civil_state_id'] = $civil_state['CivilState']['id'];
			}
			else{
				$civil_state = array(
					'CivilState' => array(
						'name' => $this->request->data['Worker']['civil_state_id'],
						)
					);
				$this->CivilState->create();
				$this->CivilState->save($civil_state);
				$this->request->data['Worker']['civil_state_id'] = $this->CivilState->id;
			}


			$bank = $this->Bank->find('first', array(
				'conditions' => array('Bank.name' => $this->request->data['Worker']['bank_id'])
				));
			if ($bank){
				$this->request->data['Worker']['bank_id'] = $bank['Bank']['id'];
			}
			else{
				$bank = array(
					'Bank' => array(
						'name' => $this->request->data['Worker']['bank_id'],
						)
					);
				$this->Bank->create();
				$this->Bank->save($bank);
				$this->request->data['Worker']['bank_id'] = $this->Bank->id;
			}

			$this->Worker->save($this->request->data);


			$degrees_array = $this->request->data['Degree'];

			foreach ($degrees_array as $key => $pre_carrer) {
				$this->request->data['Degree'][$key]['worker_id'] = $this->Worker->id;

				$carrer = $this->Career->find('first', array(
					'conditions' => array('Career.name' => $pre_carrer['career_id'])
					));
				if ($carrer){
					$this->Career->id = $carrer['Career']['id'];
				}
				else{
					$carrer = array(
						'Career' => array(
							'name' => $pre_carrer['career_id'],
							)
						);
					$this->Career->create();
					$this->Career->save($carrer);
				}


				$carrer = $this->InstructionGrade->find('first', array(
					'conditions' => array('InstructionGrade.name' => $pre_carrer['instruction_grade_id'])
					));
				if ($carrer){
					$this->InstructionGrade->id = $carrer['InstructionGrade']['id'];
				}
				else{
					$carrer = array(
						'InstructionGrade' => array(
							'name' => $pre_carrer['instruction_grade_id'],
							)
						);
					$this->InstructionGrade->create();
					$this->InstructionGrade->save($carrer);
				}

				$carrer = $this->Institution->find('first', array(
					'conditions' => array('Institution.name' => $pre_carrer['institution_id'])
					));
				if ($carrer){
					$this->Institution->id = $carrer['Institution']['id'];
				}
				else{
					$carrer = array(
						'Institution' => array(
							'name' => $pre_carrer['institution_id'],
							)
						);
					$this->Institution->create();
					$this->Institution->save($carrer);
				}


				$carrer = array(
					'Degrees' => array(
						'worker_id' => $this->Worker->id,
						'instruction_grade_id' => $this->InstructionGrade->id,
						'institution_id' => $this->Institution->id,
						'career_id' => $this->Career->id,
						'egress_date' => $pre_carrer['egress_date'],
						)
					);
				$this->Degrees->create();
				$this->Degrees->save($carrer);
			}


			$equipment = $this->Equipment->find('all');

			$degrees_array = $this->request->data['EquipmentSize'];

			foreach ($degrees_array as $key => $pre_carrer) {
				$carrer = array(
					'EquipmentSize' => array(
						'worker_id' => $this->Worker->id,
						'equipment_id' => $equipment[$key-1]['Equipment']['id'],
						'size' => $pre_carrer['size']
						)
					);
				$this->EquipmentSize->create();
				$this->EquipmentSize->save($carrer);
			}


			$carrer = array(
				'Home' => array(
					'worker_id' => $this->Worker->id,
					'tendency' => $this->request->data['Home']['tendency']
					)
				);
			$this->Home->create();
			$this->Home->save($carrer);


			$degrees_array = $this->request->data['HomesService'];

			foreach ($degrees_array as $key => $pre_carrer) {
				$carrer = array(
					'HomesService' => array(
						'home_id' => $this->Home->id,
						'service_id' => $key,
						)
					);
				$this->HomesService->create();
				$this->HomesService->save($carrer);
			}


			$this->loadModel('MedicalRegister');
			$this->loadModel('SocialRegister');

			$this->request->data['MedicalRegister']['worker_id'] = $this->Worker->id;
			$this->MedicalRegister->create();
			$this->MedicalRegister->save($this->request->data['MedicalRegister']);

			$this->request->data['SocialRegister']['worker_id'] = $this->Worker->id;
			$this->SocialRegister->create();
			$this->SocialRegister->save($this->request->data['SocialRegister']);


			$this->request->data['Family']['worker_id'] = $this->Worker->id;
			$this->Family->create();
			$this->Family->save($this->request->data);


			debug($this->request->data);
			$this->Session->setFlash(__('El trabajador ha sido guardado.'), 'success_alert');
			//return $this->redirect(array('action' => 'index'));

			/*if ($this->Worker->save($this->request->data)) {
				$this->Session->setFlash(__('El trabajador ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El trabajador no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}*/
		}
		
		$banks = $this->Worker->Bank->find('list');
		$districts = $this->Worker->District->find('list');
		$provinces = $this->Province->find('list');
		$departments = $this->Department->find('list');
		$civilStates = $this->Worker->CivilState->find('list');

		$equipment = $this->Equipment->find('list');
		$instructionGrades = $this->InstructionGrade->find('list');
		$institutions = $this->Institution->find('list');
		$careers = $this->Career->find('list');
		$services = $this->Service->find('list');
		$companies = $this->Company->find('list');
		$this->set(compact('banks', 'departments', 'provinces', 'districts', 'civilStates', 'instructionGrades', 'institutions', 'careers', 'equipment', 'services', 'companies'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Worker->exists($id)) {
			throw new NotFoundException(__('Invalid worker'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Worker->save($this->request->data)) {
				$this->Session->setFlash(__('El trabajador ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El trabajador no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('Worker.' . $this->Worker->primaryKey => $id));
			$this->request->data = $this->Worker->find('first', $options);
		}
		$banks = $this->Worker->Bank->find('list');
		$districts = $this->Worker->District->find('list');
		$civilStates = $this->Worker->CivilState->find('list');
		$this->set(compact('banks', 'districts', 'civilStates'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Worker->id = $id;
		if (!$this->Worker->exists()) {
			throw new NotFoundException(__('Invalid worker'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Worker->delete()) {
			$this->Session->setFlash(__('El trabajador ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('El trabajador no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}

}
