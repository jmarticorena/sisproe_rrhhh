<?php
App::uses('AppController', 'Controller');
/**
 * CostCenters Controller
 *
 * @property CostCenter $CostCenter
 * @property PaginatorComponent $Paginator
 */
class CostCentersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CostCenter->recursive = 0;
		$this->set('costCenters', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CostCenter->exists($id)) {
			throw new NotFoundException(__('Invalid cost center'));
		}
		$options = array('conditions' => array('CostCenter.' . $this->CostCenter->primaryKey => $id));
		$this->set('costCenter', $this->CostCenter->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CostCenter->create();
			if ($this->CostCenter->save($this->request->data)) {
				$this->Session->setFlash(__('El centro de costo ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El centro de costo no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CostCenter->exists($id)) {
			throw new NotFoundException(__('Invalid cost center'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CostCenter->save($this->request->data)) {
				$this->Session->setFlash(__('El centro de costo ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El centro de costo no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('CostCenter.' . $this->CostCenter->primaryKey => $id));
			$this->request->data = $this->CostCenter->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CostCenter->id = $id;
		if (!$this->CostCenter->exists()) {
			throw new NotFoundException(__('Invalid cost center'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CostCenter->delete()) {
			$this->Session->setFlash(__('El centro de costo ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('El centro de costo no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
