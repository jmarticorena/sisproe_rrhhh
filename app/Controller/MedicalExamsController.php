<?php
App::uses('AppController', 'Controller');
/**
 * MedicalExams Controller
 *
 * @property MedicalExam $MedicalExam
 * @property PaginatorComponent $Paginator
 */
class MedicalExamsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MedicalExam->recursive = 0;
		$this->set('medicalExams', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MedicalExam->exists($id)) {
			throw new NotFoundException(__('Invalid medical exam'));
		}
		$options = array('conditions' => array('MedicalExam.' . $this->MedicalExam->primaryKey => $id));
		$this->set('medicalExam', $this->MedicalExam->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MedicalExam->create();
			if ($this->MedicalExam->save($this->request->data)) {
				$this->Session->setFlash(__('El exámen médico ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El exámen médico no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$workers = $this->MedicalExam->Worker->find('list', array('fields'=>array('Worker.id', 'Worker.full')));
		$this->set(compact('workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MedicalExam->exists($id)) {
			throw new NotFoundException(__('Invalid medical exam'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MedicalExam->save($this->request->data)) {
				$this->Session->setFlash(__('El exámen médico ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El exámen médico no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('MedicalExam.' . $this->MedicalExam->primaryKey => $id));
			$this->request->data = $this->MedicalExam->find('first', $options);
		}
		$workers = $this->MedicalExam->Worker->find('list');
		$this->set(compact('workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MedicalExam->id = $id;
		if (!$this->MedicalExam->exists()) {
			throw new NotFoundException(__('Invalid medical exam'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->MedicalExam->delete()) {
			$this->Session->setFlash(__('El exámen médico ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('El exámen médico no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function report() {
		$this->MedicalExam->recursive = 0;
		$this->paginate = array(
			'limit' => 8,
			'order' => 'MedicalExam.end ASC'
		);
		$this->set('medicalExams', $this->Paginator->paginate());
	}

	public function report_1() {
		$this->MedicalExam->recursive = 0;

		$conditions = array(
			array('MedicalExam.end >' => date('Y-m-d', strtotime('30 day'))),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'MedicalExam.end ASC'
		);
		$this->set('medicalExams', $this->Paginator->paginate());
	}

	public function report_2() {
		$this->MedicalExam->recursive = 0;

		$conditions = array(
			array('MedicalExam.end >' => date('Y-m-d', strtotime('0 day'))),
			array('MedicalExam.end <' => date('Y-m-d', strtotime('30 day'))),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'MedicalExam.end ASC'
		);
		$this->set('medicalExams', $this->Paginator->paginate());
	}

	public function report_3() {
		$this->MedicalExam->recursive = 0;

		$actual = date('Y-m-d');

		$conditions = array(
			array('MedicalExam.end <' => $actual),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'MedicalExam.end ASC'
		);
		$this->set('medicalExams', $this->Paginator->paginate());
	}
}
