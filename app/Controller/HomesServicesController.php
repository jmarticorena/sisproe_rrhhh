<?php
App::uses('AppController', 'Controller');
/**
 * HomesServices Controller
 *
 * @property HomesService $HomesService
 * @property PaginatorComponent $Paginator
 */
class HomesServicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->HomesService->recursive = 0;
		$this->set('homesServices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->HomesService->exists($id)) {
			throw new NotFoundException(__('Invalid homes service'));
		}
		$options = array('conditions' => array('HomesService.' . $this->HomesService->primaryKey => $id));
		$this->set('homesService', $this->HomesService->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->HomesService->create();
			if ($this->HomesService->save($this->request->data)) {
				$this->Session->setFlash(__('The homes service ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homes service no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$homes = $this->HomesService->Home->find('list');
		$services = $this->HomesService->Service->find('list');
		$this->set(compact('homes', 'services'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->HomesService->exists($id)) {
			throw new NotFoundException(__('Invalid homes service'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->HomesService->save($this->request->data)) {
				$this->Session->setFlash(__('The homes service ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The homes service no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('HomesService.' . $this->HomesService->primaryKey => $id));
			$this->request->data = $this->HomesService->find('first', $options);
		}
		$homes = $this->HomesService->Home->find('list');
		$services = $this->HomesService->Service->find('list');
		$this->set(compact('homes', 'services'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->HomesService->id = $id;
		if (!$this->HomesService->exists()) {
			throw new NotFoundException(__('Invalid homes service'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->HomesService->delete()) {
			$this->Session->setFlash(__('The homes service ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The homes service no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
