<?php
App::uses('AppController', 'Controller');
/**
 * Legacies Controller
 *
 * @property Legacy $Legacy
 * @property PaginatorComponent $Paginator
 */
class LegaciesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Legacy->recursive = 0;
		$this->set('legacies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Legacy->exists($id)) {
			throw new NotFoundException(__('Invalid legacy'));
		}
		$options = array('conditions' => array('Legacy.' . $this->Legacy->primaryKey => $id));
		$this->set('legacy', $this->Legacy->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Legacy->create();
			if ($this->Legacy->save($this->request->data)) {
				$this->Session->setFlash(__('The legacy ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The legacy no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$families = $this->Legacy->Family->find('list');
		$this->set(compact('families'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Legacy->exists($id)) {
			throw new NotFoundException(__('Invalid legacy'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Legacy->save($this->request->data)) {
				$this->Session->setFlash(__('The legacy ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The legacy no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('Legacy.' . $this->Legacy->primaryKey => $id));
			$this->request->data = $this->Legacy->find('first', $options);
		}
		$families = $this->Legacy->Family->find('list');
		$this->set(compact('families'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Legacy->id = $id;
		if (!$this->Legacy->exists()) {
			throw new NotFoundException(__('Invalid legacy'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Legacy->delete()) {
			$this->Session->setFlash(__('The legacy ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The legacy no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
