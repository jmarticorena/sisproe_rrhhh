<?php
App::uses('AppController', 'Controller');
/**
 * CivilStates Controller
 *
 * @property CivilState $CivilState
 * @property PaginatorComponent $Paginator
 */
class CivilStatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CivilState->recursive = 0;
		$this->set('civilStates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CivilState->exists($id)) {
			throw new NotFoundException(__('Invalid civil state'));
		}
		$options = array('conditions' => array('CivilState.' . $this->CivilState->primaryKey => $id));
		$this->set('civilState', $this->CivilState->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CivilState->create();
			if ($this->CivilState->save($this->request->data)) {
				$this->Session->setFlash(__('The civil state ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The civil state no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CivilState->exists($id)) {
			throw new NotFoundException(__('Invalid civil state'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CivilState->save($this->request->data)) {
				$this->Session->setFlash(__('The civil state ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The civil state no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('CivilState.' . $this->CivilState->primaryKey => $id));
			$this->request->data = $this->CivilState->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CivilState->id = $id;
		if (!$this->CivilState->exists()) {
			throw new NotFoundException(__('Invalid civil state'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CivilState->delete()) {
			$this->Session->setFlash(__('The civil state ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The civil state no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
