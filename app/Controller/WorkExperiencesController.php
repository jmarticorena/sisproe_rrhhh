<?php
App::uses('AppController', 'Controller');
/**
 * WorkExperiences Controller
 *
 * @property WorkExperience $WorkExperience
 * @property PaginatorComponent $Paginator
 */
class WorkExperiencesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->WorkExperience->recursive = 0;
		$this->set('workExperiences', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->WorkExperience->exists($id)) {
			throw new NotFoundException(__('Invalid work experience'));
		}
		$options = array('conditions' => array('WorkExperience.' . $this->WorkExperience->primaryKey => $id));
		$this->set('workExperience', $this->WorkExperience->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->WorkExperience->create();
			if ($this->WorkExperience->save($this->request->data)) {
				$this->Session->setFlash(__('La experiencia laboral ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La experiencia laboral no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$companies = $this->WorkExperience->Company->find('list');
		$workers = $this->WorkExperience->Worker->find('list');
		$this->set(compact('companies', 'workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->WorkExperience->exists($id)) {
			throw new NotFoundException(__('Invalid work experience'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->WorkExperience->save($this->request->data)) {
				$this->Session->setFlash(__('La experiencia laboral ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La experiencia laboral no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('WorkExperience.' . $this->WorkExperience->primaryKey => $id));
			$this->request->data = $this->WorkExperience->find('first', $options);
		}
		$companies = $this->WorkExperience->Company->find('list');
		$workers = $this->WorkExperience->Worker->find('list');
		$this->set(compact('companies', 'workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->WorkExperience->id = $id;
		if (!$this->WorkExperience->exists()) {
			throw new NotFoundException(__('Invalid work experience'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->WorkExperience->delete()) {
			$this->Session->setFlash(__('La experiencia laboral ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('La experiencia laboral no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
