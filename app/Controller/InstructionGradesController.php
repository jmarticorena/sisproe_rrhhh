<?php
App::uses('AppController', 'Controller');
/**
 * InstructionGrades Controller
 *
 * @property InstructionGrade $InstructionGrade
 * @property PaginatorComponent $Paginator
 */
class InstructionGradesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InstructionGrade->recursive = 0;
		$this->set('instructionGrades', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InstructionGrade->exists($id)) {
			throw new NotFoundException(__('Invalid instruction grade'));
		}
		$options = array('conditions' => array('InstructionGrade.' . $this->InstructionGrade->primaryKey => $id));
		$this->set('instructionGrade', $this->InstructionGrade->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InstructionGrade->create();
			if ($this->InstructionGrade->save($this->request->data)) {
				$this->Session->setFlash(__('The instruction grade ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The instruction grade no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InstructionGrade->exists($id)) {
			throw new NotFoundException(__('Invalid instruction grade'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InstructionGrade->save($this->request->data)) {
				$this->Session->setFlash(__('The instruction grade ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The instruction grade no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('InstructionGrade.' . $this->InstructionGrade->primaryKey => $id));
			$this->request->data = $this->InstructionGrade->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InstructionGrade->id = $id;
		if (!$this->InstructionGrade->exists()) {
			throw new NotFoundException(__('Invalid instruction grade'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InstructionGrade->delete()) {
			$this->Session->setFlash(__('The instruction grade ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The instruction grade no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
