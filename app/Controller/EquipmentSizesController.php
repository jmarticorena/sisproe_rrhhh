<?php
App::uses('AppController', 'Controller');
/**
 * EquipmentSizes Controller
 *
 * @property EquipmentSize $EquipmentSize
 * @property PaginatorComponent $Paginator
 */
class EquipmentSizesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->EquipmentSize->recursive = 0;
		$this->set('equipmentSizes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EquipmentSize->exists($id)) {
			throw new NotFoundException(__('Invalid equipment size'));
		}
		$options = array('conditions' => array('EquipmentSize.' . $this->EquipmentSize->primaryKey => $id));
		$this->set('equipmentSize', $this->EquipmentSize->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->EquipmentSize->create();
			if ($this->EquipmentSize->save($this->request->data)) {
				$this->Session->setFlash(__('The equipment size ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The equipment size no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$equipment = $this->EquipmentSize->Equipment->find('list');
		$workers = $this->EquipmentSize->Worker->find('list');
		$this->set(compact('equipment', 'workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EquipmentSize->exists($id)) {
			throw new NotFoundException(__('Invalid equipment size'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EquipmentSize->save($this->request->data)) {
				$this->Session->setFlash(__('The equipment size ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The equipment size no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('EquipmentSize.' . $this->EquipmentSize->primaryKey => $id));
			$this->request->data = $this->EquipmentSize->find('first', $options);
		}
		$equipment = $this->EquipmentSize->Equipment->find('list');
		$workers = $this->EquipmentSize->Worker->find('list');
		$this->set(compact('equipment', 'workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->EquipmentSize->id = $id;
		if (!$this->EquipmentSize->exists()) {
			throw new NotFoundException(__('Invalid equipment size'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->EquipmentSize->delete()) {
			$this->Session->setFlash(__('The equipment size ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The equipment size no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
