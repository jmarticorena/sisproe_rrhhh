<?php
App::uses('AppController', 'Controller');
/**
 * Homes Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 */
class HomesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Home->recursive = 0;
		$this->set('homes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Home->exists($id)) {
			throw new NotFoundException(__('Invalid home'));
		}
		$options = array('conditions' => array('Home.' . $this->Home->primaryKey => $id));
		$this->set('home', $this->Home->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Home->create();
			if ($this->Home->save($this->request->data)) {
				$this->Session->setFlash(__('The home ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The home no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$workers = $this->Home->Worker->find('list');
		$services = $this->Home->Service->find('list');
		$this->set(compact('workers', 'services'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Home->exists($id)) {
			throw new NotFoundException(__('Invalid home'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Home->save($this->request->data)) {
				$this->Session->setFlash(__('The home ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The home no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('Home.' . $this->Home->primaryKey => $id));
			$this->request->data = $this->Home->find('first', $options);
		}
		$workers = $this->Home->Worker->find('list');
		$services = $this->Home->Service->find('list');
		$this->set(compact('workers', 'services'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Home->id = $id;
		if (!$this->Home->exists()) {
			throw new NotFoundException(__('Invalid home'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Home->delete()) {
			$this->Session->setFlash(__('The home ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The home no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
