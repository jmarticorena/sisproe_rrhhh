<?php
App::uses('AppController', 'Controller');
/**
 * Contracts Controller
 *
 * @property Contract $Contract
 * @property PaginatorComponent $Paginator
 */
class ContractsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Contract->recursive = 0;
		$this->set('contracts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Contract->exists($id)) {
			throw new NotFoundException(__('Invalid contract'));
		}
		$options = array('conditions' => array('Contract.' . $this->Contract->primaryKey => $id));
		$this->set('contract', $this->Contract->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Contract->create();
			if ($this->Contract->save($this->request->data)) {
				$this->Session->setFlash(__('El contrato ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El contrato no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$costCenters = $this->Contract->CostCenter->find('list');
		$projects = $this->Contract->Project->find('list');
		

		$workers = $this->Contract->Worker->find('list', array('fields'=>array('Worker.id', 'Worker.full')));

		$this->set(compact('costCenters', 'projects', 'workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Contract->exists($id)) {
			throw new NotFoundException(__('Invalid contract'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Contract->save($this->request->data)) {
				$this->Session->setFlash(__('El contrato ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El contrato no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('Contract.' . $this->Contract->primaryKey => $id));
			$this->request->data = $this->Contract->find('first', $options);
		}
		$costCenters = $this->Contract->CostCenter->find('list');
		$projects = $this->Contract->Project->find('list');
		$workers = $this->Contract->Worker->find('list');
		$this->set(compact('costCenters', 'projects', 'workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Contract->id = $id;
		if (!$this->Contract->exists()) {
			throw new NotFoundException(__('Invalid contract'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Contract->delete()) {
			$this->Session->setFlash(__('El contrato ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('El contrato no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function report() {
		$this->Contract->recursive = 0;
		$this->paginate = array(
			'limit' => 8,
			'order' => 'Contract.contract_end ASC'
		);
		$this->set('contracts', $this->Paginator->paginate());
	}

	public function report_1() {
		$this->Contract->recursive = 0;

		$conditions = array(
			array('Contract.contract_end >' => date('Y-m-d', strtotime('30 day'))),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'Contract.contract_end ASC'
		);
		$this->set('contracts', $this->Paginator->paginate());
	}

	public function report_2() {
		$this->Contract->recursive = 0;

		$conditions = array(
			array('Contract.contract_end >' => date('Y-m-d', strtotime('0 day'))),
			array('Contract.contract_end <' => date('Y-m-d', strtotime('30 day'))),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'Contract.contract_end ASC'
		);
		$this->set('contracts', $this->Paginator->paginate());
	}

	public function report_3() {
		$this->Contract->recursive = 0;

		$actual = date('Y-m-d');

		$conditions = array(
			array('Contract.contract_end <' => $actual),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'Contract.contract_end ASC'
		);
		$this->set('contracts', $this->Paginator->paginate());
	}
}
