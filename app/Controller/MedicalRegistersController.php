<?php
App::uses('AppController', 'Controller');
/**
 * MedicalRegisters Controller
 *
 * @property MedicalRegister $MedicalRegister
 * @property PaginatorComponent $Paginator
 */
class MedicalRegistersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->MedicalRegister->recursive = 0;
		$this->set('medicalRegisters', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->MedicalRegister->exists($id)) {
			throw new NotFoundException(__('Invalid medical register'));
		}
		$options = array('conditions' => array('MedicalRegister.' . $this->MedicalRegister->primaryKey => $id));
		$this->set('medicalRegister', $this->MedicalRegister->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->MedicalRegister->create();
			if ($this->MedicalRegister->save($this->request->data)) {
				$this->Session->setFlash(__('The medical register ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The medical register no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$workers = $this->MedicalRegister->Worker->find('list');
		$this->set(compact('workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->MedicalRegister->exists($id)) {
			throw new NotFoundException(__('Invalid medical register'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MedicalRegister->save($this->request->data)) {
				$this->Session->setFlash(__('The medical register ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The medical register no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('MedicalRegister.' . $this->MedicalRegister->primaryKey => $id));
			$this->request->data = $this->MedicalRegister->find('first', $options);
		}
		$workers = $this->MedicalRegister->Worker->find('list');
		$this->set(compact('workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->MedicalRegister->id = $id;
		if (!$this->MedicalRegister->exists()) {
			throw new NotFoundException(__('Invalid medical register'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->MedicalRegister->delete()) {
			$this->Session->setFlash(__('The medical register ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The medical register no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
