<?php
App::uses('AppController', 'Controller');
/**
 * Insurances Controller
 *
 * @property Insurance $Insurance
 * @property PaginatorComponent $Paginator
 */
class InsurancesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Insurance->recursive = 0;
		$this->set('insurances', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Insurance->exists($id)) {
			throw new NotFoundException(__('Invalid insurance'));
		}
		$options = array('conditions' => array('Insurance.' . $this->Insurance->primaryKey => $id));
		$this->set('insurance', $this->Insurance->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Insurance->create();
			if ($this->Insurance->save($this->request->data)) {
				$this->Session->setFlash(__('El seguro ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El seguro no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$workers = $this->Insurance->Worker->find('list', array('fields'=>array('Worker.id', 'Worker.full')));
		$this->set(compact('workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Insurance->exists($id)) {
			throw new NotFoundException(__('Invalid insurance'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Insurance->save($this->request->data)) {
				$this->Session->setFlash(__('El seguro ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El seguro no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('Insurance.' . $this->Insurance->primaryKey => $id));
			$this->request->data = $this->Insurance->find('first', $options);
		}
		$workers = $this->Insurance->Worker->find('list');
		$this->set(compact('workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Insurance->id = $id;
		if (!$this->Insurance->exists()) {
			throw new NotFoundException(__('Invalid insurance'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Insurance->delete()) {
			$this->Session->setFlash(__('El seguro ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('El seguro no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function report() {
		$this->Insurance->recursive = 0;
		$this->paginate = array(
			'limit' => 8,
			'order' => 'Insurance.end ASC'
		);
		$this->set('insurances', $this->Paginator->paginate());
	}

	public function report_1() {
		$this->Insurance->recursive = 0;

		$conditions = array(
			array('Insurance.end >' => date('Y-m-d', strtotime('30 day'))),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'Insurance.end ASC'
		);
		$this->set('insurances', $this->Paginator->paginate());
	}

	public function report_2() {
		$this->Insurance->recursive = 0;

		$conditions = array(
			array('Insurance.end >' => date('Y-m-d', strtotime('0 day'))),
			array('Insurance.end <' => date('Y-m-d', strtotime('30 day'))),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'Insurance.end ASC'
		);
		$this->set('insurances', $this->Paginator->paginate());
	}

	public function report_3() {
		$this->Insurance->recursive = 0;

		$actual = date('Y-m-d');

		$conditions = array(
			array('Insurance.end <' => $actual),
			);
		$this->paginate = array(
			'limit' => 8,
			'conditions' => $conditions,
			'order' => 'Insurance.end ASC'
		);
		$this->set('insurances', $this->Paginator->paginate());
	}
}
