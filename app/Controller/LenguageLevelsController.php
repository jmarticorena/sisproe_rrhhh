<?php
App::uses('AppController', 'Controller');
/**
 * LenguageLevels Controller
 *
 * @property LenguageLevel $LenguageLevel
 * @property PaginatorComponent $Paginator
 */
class LenguageLevelsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LenguageLevel->recursive = 0;
		$this->set('lenguageLevels', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LenguageLevel->exists($id)) {
			throw new NotFoundException(__('Invalid lenguage level'));
		}
		$options = array('conditions' => array('LenguageLevel.' . $this->LenguageLevel->primaryKey => $id));
		$this->set('lenguageLevel', $this->LenguageLevel->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->LenguageLevel->create();
			if ($this->LenguageLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The lenguage level ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lenguage level no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$languages = $this->LenguageLevel->Language->find('list');
		$workers = $this->LenguageLevel->Worker->find('list');
		$this->set(compact('languages', 'workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LenguageLevel->exists($id)) {
			throw new NotFoundException(__('Invalid lenguage level'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LenguageLevel->save($this->request->data)) {
				$this->Session->setFlash(__('The lenguage level ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lenguage level no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('LenguageLevel.' . $this->LenguageLevel->primaryKey => $id));
			$this->request->data = $this->LenguageLevel->find('first', $options);
		}
		$languages = $this->LenguageLevel->Language->find('list');
		$workers = $this->LenguageLevel->Worker->find('list');
		$this->set(compact('languages', 'workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LenguageLevel->id = $id;
		if (!$this->LenguageLevel->exists()) {
			throw new NotFoundException(__('Invalid lenguage level'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->LenguageLevel->delete()) {
			$this->Session->setFlash(__('The lenguage level ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The lenguage level no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
