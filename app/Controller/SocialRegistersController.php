<?php
App::uses('AppController', 'Controller');
/**
 * SocialRegisters Controller
 *
 * @property SocialRegister $SocialRegister
 * @property PaginatorComponent $Paginator
 */
class SocialRegistersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SocialRegister->recursive = 0;
		$this->set('socialRegisters', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SocialRegister->exists($id)) {
			throw new NotFoundException(__('Invalid social register'));
		}
		$options = array('conditions' => array('SocialRegister.' . $this->SocialRegister->primaryKey => $id));
		$this->set('socialRegister', $this->SocialRegister->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SocialRegister->create();
			if ($this->SocialRegister->save($this->request->data)) {
				$this->Session->setFlash(__('The social register ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social register no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$workers = $this->SocialRegister->Worker->find('list');
		$this->set(compact('workers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SocialRegister->exists($id)) {
			throw new NotFoundException(__('Invalid social register'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SocialRegister->save($this->request->data)) {
				$this->Session->setFlash(__('The social register ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social register no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('SocialRegister.' . $this->SocialRegister->primaryKey => $id));
			$this->request->data = $this->SocialRegister->find('first', $options);
		}
		$workers = $this->SocialRegister->Worker->find('list');
		$this->set(compact('workers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SocialRegister->id = $id;
		if (!$this->SocialRegister->exists()) {
			throw new NotFoundException(__('Invalid social register'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SocialRegister->delete()) {
			$this->Session->setFlash(__('The social register ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The social register no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
