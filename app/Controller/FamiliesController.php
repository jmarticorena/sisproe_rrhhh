<?php
App::uses('AppController', 'Controller');
/**
 * Families Controller
 *
 * @property Family $Family
 * @property PaginatorComponent $Paginator
 */
class FamiliesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Family->recursive = 0;
		$this->set('families', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Family->exists($id)) {
			throw new NotFoundException(__('Invalid family'));
		}
		$options = array('conditions' => array('Family.' . $this->Family->primaryKey => $id));
		$this->set('family', $this->Family->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Family->create();
			if ($this->Family->save($this->request->data)) {
				$this->Session->setFlash(__('The family ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The family no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		}
		$workers = $this->Family->Worker->find('list');
		$companies = $this->Family->Company->find('list');
		$this->set(compact('workers', 'companies'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Family->exists($id)) {
			throw new NotFoundException(__('Invalid family'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Family->save($this->request->data)) {
				$this->Session->setFlash(__('The family ha sido guardado.'), 'success_alert');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The family no fue guardado. Por favor, intente denuevo.'), 'error_alert');
			}
		} else {
			$options = array('conditions' => array('Family.' . $this->Family->primaryKey => $id));
			$this->request->data = $this->Family->find('first', $options);
		}
		$workers = $this->Family->Worker->find('list');
		$companies = $this->Family->Company->find('list');
		$this->set(compact('workers', 'companies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Family->id = $id;
		if (!$this->Family->exists()) {
			throw new NotFoundException(__('Invalid family'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Family->delete()) {
			$this->Session->setFlash(__('The family ha sido eliminado.'), 'success_alert');
		} else {
			$this->Session->setFlash(__('The family no fue eliminado. Por favor, intente denuevo.'), 'error_alert');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
